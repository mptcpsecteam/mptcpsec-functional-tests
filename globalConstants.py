# VM parameters
USER_NAME = "mininet"

# Files and directories
REMOTE_FOLDER = "/home/mininet"

TEST_FOLDER = REMOTE_FOLDER + "/" + "tests"
TEST_MPTCP_SEC = "mptcpsec"
TEST_MPTCP_ESN = "mptcpesn"
TEST_SOCK_OPTS = "sockopts"
TEST_SYS_CTL = "sysctl"
TEST_FILE = "runChecks"

KERNEL_DEB_PACKAGE_FOLDER = "kerneldeb"

WIRESHARK_COMPILATION_LOG_FILE = "wireshark_compilation_error.log"

ERROR_LOG = "kernel_error.log"
