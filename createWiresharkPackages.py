#!/usr/bin/env python3

import sys
import os
import time
import subprocess

from globalConstants import WIRESHARK_COMPILATION_LOG_FILE


def show_help():
    print("This program takes the following arguments :")
    print("\tThe first argument is the path to the Wireshark source folder")
    print("\tfollowed by \"sudo apt-get build-dep wireshark\" for the dependencies")


# Arguments parsing
if len(sys.argv) == 1 or sys.argv[1] == "-h" or len(sys.argv) < 2:
    show_help()
    sys.exit(1)

if not os.path.isdir(sys.argv[1]):
    print("ERROR : Wireshark source folder " + sys.argv[1] + " doesn't exist !")
    show_help()
    sys.exit(1)
wireshark_source_folder = sys.argv[1]


start_execution = time.time()


# Start packaging
pwd = os.getcwd()
os.chdir(wireshark_source_folder)

print("debuild", "-b", "-uc", "-us")
process = subprocess.Popen(["debuild", "-b", "-uc", "-us"],
                           stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
stdout, stderr = process.communicate()
os.chdir(pwd)

if process.returncode != 0:
    print("The Wireshark compilation has failed. Its error log will be saved in " + pwd + "/" +
          WIRESHARK_COMPILATION_LOG_FILE)
    error_log = open(WIRESHARK_COMPILATION_LOG_FILE, "w")
    error_log.write(stderr)
    error_log.write(stdout)
    error_log.close()
    sys.exit(1)

print("The Wireshark compilation has succeeded. Its error log will be saved in " + pwd + "/" +
      WIRESHARK_COMPILATION_LOG_FILE)
error_log = open(WIRESHARK_COMPILATION_LOG_FILE, "w")
error_log.write(stderr)
error_log.close()


execution_time = int(time.time() - start_execution)

print("\nThe whole script took " + str(execution_time) + " seconds.")
