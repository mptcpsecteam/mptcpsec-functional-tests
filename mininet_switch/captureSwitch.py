from mininet.node import OVSKernelSwitch
from signal import SIGINT
from time import sleep


class CaptureSwitch(OVSKernelSwitch):

    def remove_firewall_rules(self):
        """
        Remove all the firewall rules that might have been set for this host
        """
        self.cmd(["iptables", "-F"])

    def start_capture(self, interface, capture_file, capture_filters=None):
        """
        Start a packet capture for every interface in the interface list

        :param interface:
        :param capture_file:
        :param capture_filters:
        :return: The process object in our namespace
        """
        open(capture_file, "w").close()
        cmd = ["tcpdump", "-i", str(interface), "-w", str(capture_file)]
        if capture_filters is not None:
            cmd.append(capture_filters)
        process = self.popen(cmd, universal_newlines=True)
        sleep(4)
        return process

    @staticmethod
    def stop_capture(capture_process):

        if capture_process.poll() is None:
            capture_process.send_signal(SIGINT)
        return capture_process.wait()
