import filecmp

from mptcpesn.mptcpesnPacketCheck import MPTCPesnPacketTestCase


class MPTCPsecPacketTestCase(MPTCPesnPacketTestCase):

    def check_data_encryption(self, capture_file, source_file, aead_algorithm):

        pass  # TODO

    def check_options_authentication(self, capture_file, aead_algorithm):

        # TODO The DSS should always be on 64 bits and DATA-ACK too

        # TODO Always a DSS !

        pass  # TODO

    def check_closed_subflow(self, capture_file):

        self.check_rst(capture_file)

    def check_transfer_success(self, source_file, destination_file):

        self.assertTrue(filecmp.cmp(source_file, destination_file, shallow=0),
                        msg="The transferred file is not strictly identical to the source file !")

    def check_transfer_failure(self, source_file, destination_file):

        self.assertFalse(filecmp.cmp(source_file, destination_file, shallow=0),
                         msg="The transferred file is strictly identical to the source file !")
