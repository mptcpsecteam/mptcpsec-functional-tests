from cryptography_api.aead import AEAD_AES_128_GCM
from mptcpesn.logger import Logger
from mptcpsec.mptcpsecClientServerCheck import MPTCPsecClientServerTestCase
from scripts.packetFormat import ESN_SUBOPTIONS
from sysctl.sysctlVariables import get_value_from_sysctl, set_sysctl_value, MPTCP_ESN_PREFERENCE_LIST, \
    MPTCP_VERSION, MPTCP_VERSION_2, MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256, MPTCP_PATH_MANAGER, \
    MPTCP_DEFAULT_PATH_MANAGER, MPTCP_FULLMESH_PATH_MANAGER, MPTCP_DSS_CHECKSUM


class MPTCPsecTest(MPTCPsecClientServerTestCase, Logger):

    def get_log_folder(self):
        return self.log_folder

    def setUp(self):

        self.mptcp_esn_preference_list_old = get_value_from_sysctl(MPTCP_ESN_PREFERENCE_LIST)
        set_sysctl_value(MPTCP_ESN_PREFERENCE_LIST, "".join(ESN_SUBOPTIONS))

        self.mptcp_version_old = get_value_from_sysctl(MPTCP_VERSION)
        set_sysctl_value(MPTCP_VERSION, MPTCP_VERSION_2)

        self.mptcp_path_manager_old = get_value_from_sysctl(MPTCP_PATH_MANAGER)
        set_sysctl_value(MPTCP_PATH_MANAGER, MPTCP_DEFAULT_PATH_MANAGER)

        self.mptcp_dss_checksum_old = get_value_from_sysctl(MPTCP_DSS_CHECKSUM)
        set_sysctl_value(MPTCP_DSS_CHECKSUM, "0")

        # Remove all firewall rules
        self.client.remove_firewall_rules()
        self.server.remove_firewall_rules()
        self.router.remove_firewall_rules()

        # Allow forwarding on the router
        self.router.stop_attacks()

    def test_regression(self, log_prefix="regression"):

        encryption_scheme = MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256
        transfer_timeout = 20  # seconds

        # files = ["input_files/tiny_ipsum.txt", "input_files/ipsum.txt", "input_files/longer_ipsum.txt",
        #         "input_files/huge_ipsum.txt", "input_files/bravo.jpeg", "input_files/frankenstein-its-alive.gif",
        #         "input_files/i-iz-sorry_o_203383.jpg", "input_files/joker_mind.jpg", "input_files/pave_cesar.jpg",
        #         "input_files/veuxPas.jpg"]
        files = ["input_files/frankenstein-its-alive.gif"]
        aead_algorithms = [AEAD_AES_128_GCM]
        i = 1
        for aead_algorithm in aead_algorithms:
            for source_file in files:
                print("Current file : " + source_file)
                log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, i)
                self.transfer_data(source_file=source_file, encryption_scheme=encryption_scheme,
                                   aead_algorithm=aead_algorithm, transfer_timeout=transfer_timeout,
                                   log_folder=log_sub_folder, mptcp=True)
                i += 1

    def test_regression_several_subflows(self):

        set_sysctl_value(MPTCP_PATH_MANAGER, MPTCP_FULLMESH_PATH_MANAGER)

        self.test_regression(log_prefix="regression_several_subflows")

        set_sysctl_value(MPTCP_PATH_MANAGER, MPTCP_DEFAULT_PATH_MANAGER)

    def test_normal_behaviour(self, log_prefix="normal_behaviour"):

        encryption_scheme = MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256
        transfer_timeout = 20  # seconds

        # files = ["input_files/tiny_ipsum.txt", "input_files/ipsum.txt", "input_files/longer_ipsum.txt",
        #         "input_files/huge_ipsum.txt", "input_files/bravo.jpeg", "input_files/frankenstein-its-alive.gif",
        #         "input_files/i-iz-sorry_o_203383.jpg", "input_files/joker_mind.jpg", "input_files/pave_cesar.jpg",
        #         "input_files/veuxPas.jpg"]
        files = ["input_files/frankenstein-its-alive.gif"]
        aead_algorithms = [AEAD_AES_128_GCM]
        i = 1
        for aead_algorithm in aead_algorithms:
            for source_file in files:
                print("Current file : " + source_file)
                log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, i)
                self.transfer_data(source_file=source_file, encryption_scheme=encryption_scheme,
                                   aead_algorithm=aead_algorithm, transfer_timeout=transfer_timeout,
                                   log_folder=log_sub_folder)
                i += 1

    def test_normal_behaviour_several_subflows(self):

        set_sysctl_value(MPTCP_PATH_MANAGER, MPTCP_FULLMESH_PATH_MANAGER)

        self.test_normal_behaviour(log_prefix="normal_behaviour_several_subflows")

        set_sysctl_value(MPTCP_PATH_MANAGER, MPTCP_DEFAULT_PATH_MANAGER)

    def test_attacks_several_subflows(self):

        set_sysctl_value(MPTCP_PATH_MANAGER, MPTCP_FULLMESH_PATH_MANAGER)

        encryption_scheme = MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256
        transfer_timeout = 20  # seconds

        log_prefix = "attacks_several_subflows"

        # files = ["input_files/bravo.jpeg", "input_files/frankenstein-its-alive.gif",
        #         "input_files/i-iz-sorry_o_203383.jpg", "input_files/joker_mind.jpg", "input_files/pave_cesar.jpg",
        #         "input_files/veuxPas.jpg"]
        files = ["input_files/frankenstein-its-alive.gif"]
        aead_algorithms = [AEAD_AES_128_GCM]
        i = 1
        for aead_algorithm in aead_algorithms:
            for source_file in files:
                print("Current file : " + source_file)
                log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, i)
                self.transfer_data_with_data_corruption(source_file=source_file, encryption_scheme=encryption_scheme,
                                                        aead_algorithm=aead_algorithm,
                                                        transfer_timeout=transfer_timeout, log_folder=log_sub_folder)
                i += 1

        set_sysctl_value(MPTCP_PATH_MANAGER, MPTCP_DEFAULT_PATH_MANAGER)

    def test_attacks_tls(self):

        set_sysctl_value(MPTCP_PATH_MANAGER, MPTCP_FULLMESH_PATH_MANAGER)
        # In order not to be forced to modify manually the DSS checksum, we deactivate the MPTCP checksum
        set_sysctl_value(MPTCP_DSS_CHECKSUM, "0")

        encryption_scheme = MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256
        transfer_timeout = 20  # seconds

        log_prefix = "tls_attacks_several_subflows"

        files = ["input_files/frankenstein-its-alive.gif"]
        aead_algorithms = [AEAD_AES_128_GCM]
        i = 1
        for aead_algorithm in aead_algorithms:
            for source_file in files:
                print("Current file : " + source_file)
                log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, i)
                self.tls_transfer_data_with_data_corruption(source_file=source_file,
                                                            encryption_scheme=encryption_scheme,
                                                            aead_algorithm=aead_algorithm,
                                                            transfer_timeout=transfer_timeout,
                                                            log_folder=log_sub_folder)
                i += 1

        set_sysctl_value(MPTCP_PATH_MANAGER, MPTCP_DEFAULT_PATH_MANAGER)
        set_sysctl_value(MPTCP_DSS_CHECKSUM, "1")

    def tearDown(self):

        set_sysctl_value(MPTCP_ESN_PREFERENCE_LIST, self.mptcp_esn_preference_list_old)

        set_sysctl_value(MPTCP_VERSION, self.mptcp_version_old)

        set_sysctl_value(MPTCP_PATH_MANAGER, self.mptcp_path_manager_old)

        set_sysctl_value(MPTCP_DSS_CHECKSUM, self.mptcp_dss_checksum_old)

        # Remove all firewall rules
        self.client.remove_firewall_rules()
        self.server.remove_firewall_rules()
        self.router.remove_firewall_rules()

        # Allow forwarding on the router
        self.router.stop_attacks()
