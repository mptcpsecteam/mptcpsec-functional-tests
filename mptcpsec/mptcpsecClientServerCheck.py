import os
from mininet.net import Mininet
from subprocess import Popen
from time import sleep

from mininet_host import MPTCPsecHost
from mininet_switch import CaptureSwitch
from mininet_topo import RouterMultiPathTopo
from mptcpsec.mptcpsecPacketCheck import MPTCPsecPacketTestCase
from sockopts.sockOptions import MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_NOT
from sysctl.sysctlVariables import MPTCP_ESN_PREFERENCE_LIST

TIMEOUT = 10  # Time (in seconds) between the launching of each command


class MPTCPsecClientServerTestCase(MPTCPsecPacketTestCase):

    topology = None
    network = None
    client = None
    server = None
    router = None
    log_folder = "mptcpsec/log"

    @classmethod
    def enable_all_paths(cls):

        cls.client.setup_routes([cls.router])
        cls.server.setup_routes([cls.router])

    @classmethod
    def setUpClass(cls):
        """
        Setup the mininet network
        """

        file_obj = open("clean_mininet.log", "w")
        process = Popen(["sudo", "mn", "-c", "-v", "info"], stdout=file_obj, stderr=file_obj)  # Clean mininet files
        process.wait()
        file_obj.close()
        cls.topology = RouterMultiPathTopo(hopts={"cls": MPTCPsecHost}, sopts={"cls": CaptureSwitch})
        cls.network = Mininet(cls.topology)
        cls.client = cls.network.getNodeByName("client")
        cls.server = cls.network.getNodeByName("server")
        cls.server.setIP("172.16.0.100/24", intf=cls.server.intf("server-eth1"))  # TODO important (Does not yet work !)
        cls.switch1 = cls.network.getNodeByName("s1")
        cls.switch2 = cls.network.getNodeByName("s2")
        cls.switch3 = cls.network.getNodeByName("s3")
        cls.router = cls.network.getNodeByName("router")

        cls.network.start()
        cls.enable_all_paths()

        cls.switch3_intf_to_client = cls.switch3.connectionsTo(cls.client)[0][0].name

    def transfer_data(self, source_file, encryption_scheme, aead_algorithm,
                      transfer_timeout, log_folder=log_folder, mptcp=False):
        """
        This method will perform the following test :
            - Transfer the content of a file to another one (and check the equivalence of the bytes)
        Please enter a sufficiently large amount of time for the timeout.
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "transfer_data.pcap")

        if mptcp:
            sock_opt_client_conf = [(MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_NOT)]
            sock_opt_server_conf = [(MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_NOT)]
        else:
            sock_opt_client_conf = None
            sock_opt_server_conf = None

        # Set the correct MPTCPsec encryption specification
        sys_ctl_client_conf = [(MPTCP_ESN_PREFERENCE_LIST, encryption_scheme)]
        sys_ctl_server_conf = [(MPTCP_ESN_PREFERENCE_LIST, encryption_scheme)]

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        tcp_client_process = None
        try:
            tcp_server_process = self.server.listen_and_save_file(local_ip=self.server.IP(), local_port=server_port,
                                                                  sys_ctl_conf=sys_ctl_server_conf,
                                                                  sock_opt_conf=sock_opt_server_conf,
                                                                  file_path=source_file,
                                                                  log_folder=log_folder, mptcp=mptcp)
            sleep(timeout)
            tcp_client_process = self.client.connect_and_transfer_file(remote_ip=self.server.IP(),
                                                                       remote_port=server_port,
                                                                       connection_timeout=timeout,
                                                                       sys_ctl_conf=sys_ctl_client_conf,
                                                                       sock_opt_conf=sock_opt_client_conf,
                                                                       file_path=source_file,
                                                                       log_folder=log_folder, mptcp=mptcp)
            sleep(transfer_timeout)
            self.switch3.stop_capture(process)

            if not mptcp:

                # Check the encryption scheme chosen
                client_encryption_scheme = self.client.get_client_negotiated_value(tcp_client_process,
                                                                                   log_folder=log_folder)
                server_encryption_scheme = self.server.get_server_negotiated_value(tcp_server_process,
                                                                                   log_folder=log_folder)
                self.check_mptcp_esn_connection(capture_file=capture_file, server_ip=self.server.IP(),
                                                server_port=server_port,
                                                client_encryption_scheme=client_encryption_scheme,
                                                server_encryption_scheme=server_encryption_scheme,
                                                sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=None,
                                                sys_ctl_server_conf=sys_ctl_server_conf, sock_opt_server_conf=None)

                self.check_data_encryption(capture_file=capture_file, source_file=source_file,
                                           aead_algorithm=aead_algorithm)  # TODO
                self.check_options_authentication(capture_file=capture_file,
                                                  aead_algorithm=aead_algorithm)  # TODO
            destination_file = os.path.join(log_folder, os.path.basename(source_file))
            self.check_transfer_success(source_file=source_file, destination_file=destination_file)
        finally:
            MPTCPsecHost.kill_all(log_folder, tcp_server_process, tcp_client_process)
            self.switch3.stop_capture(process)

    def transfer_data_with_data_corruption(self, source_file, encryption_scheme, aead_algorithm,
                                           transfer_timeout, log_folder=log_folder):
        """
        This method will perform the following test with an attacker that will modify data on one of the subflows :
            - Transfer the content of a file to another one (and check the equivalence of the bytes)
        Please enter a sufficiently large amount of time for the timeout.
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "transfer_data.pcap")

        # Set the correct MPTCPsec encryption specification
        sys_ctl_client_conf = [(MPTCP_ESN_PREFERENCE_LIST, encryption_scheme)]
        sys_ctl_server_conf = [(MPTCP_ESN_PREFERENCE_LIST, encryption_scheme)]

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        tcp_client_process = None
        attacker_process = None
        try:
            attacker_process = self.router.corrupt_data(wait_for_two_subflows=True)
            tcp_server_process = self.server.listen_and_save_file(local_ip=self.server.IP(), local_port=server_port,
                                                                  sys_ctl_conf=sys_ctl_server_conf,
                                                                  sock_opt_conf=None, file_path=source_file,
                                                                  log_folder=log_folder)
            sleep(timeout)
            tcp_client_process = self.client.connect_and_transfer_file(remote_ip=self.server.IP(),
                                                                       remote_port=server_port,
                                                                       connection_timeout=timeout,
                                                                       sys_ctl_conf=sys_ctl_client_conf,
                                                                       sock_opt_conf=None, file_path=source_file,
                                                                       log_folder=log_folder)
            sleep(transfer_timeout)
            self.switch3.stop_capture(process)

            # Check the encryption scheme chosen
            client_encryption_scheme = self.client.get_client_negotiated_value(tcp_client_process,
                                                                               log_folder=log_folder)
            server_encryption_scheme = self.server.get_server_negotiated_value(tcp_server_process,
                                                                               log_folder=log_folder)
            self.check_mptcp_esn_connection(capture_file=capture_file, server_ip=self.server.IP(),
                                            server_port=server_port, client_encryption_scheme=client_encryption_scheme,
                                            server_encryption_scheme=server_encryption_scheme,
                                            sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=None,
                                            sys_ctl_server_conf=sys_ctl_server_conf, sock_opt_server_conf=None)

            self.check_data_encryption(capture_file=capture_file, source_file=source_file,
                                       aead_algorithm=aead_algorithm)  # TODO
            self.check_options_authentication(capture_file=capture_file,
                                              aead_algorithm=aead_algorithm)  # TODO
            self.check_closed_subflow(capture_file=capture_file)
            destination_file = os.path.join(log_folder, os.path.basename(source_file))
            self.check_transfer_success(source_file=source_file, destination_file=destination_file)
        finally:
            MPTCPsecHost.kill_all(log_folder, tcp_server_process, tcp_client_process, attacker_process)
            self.switch3.stop_capture(process)
            self.router.stop_attacks()

    def tls_transfer_data_with_data_corruption(self, source_file, encryption_scheme, aead_algorithm, transfer_timeout,
                                               log_folder=log_folder):

        """
        This method will perform the following test with an attacker that will modify data on one of the subflows :
            - Transfer the content of a file to another one (and check the equivalence of the bytes)
        Please enter a sufficiently large amount of time for the timeout.
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "transfer_data.pcap")

        # Set the correct MPTCPsec encryption specification
        sock_opt_client_conf = [(MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_NOT)]
        sock_opt_server_conf = [(MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_NOT)]

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        tcp_client_process = None
        attacker_process = None
        try:
            attacker_process = self.router.corrupt_data(wait_for_two_subflows=True)
            tcp_server_process = self.server.tls_listen_and_save_file(local_ip=self.server.IP(),
                                                                      local_port=server_port,
                                                                      sock_opt_conf=sock_opt_server_conf,
                                                                      file_path=source_file,
                                                                      log_folder=log_folder)
            sleep(timeout)
            tcp_client_process = self.client.tls_connect_and_transfer_file(remote_ip=self.server.IP(),
                                                                           remote_port=server_port,
                                                                           connection_timeout=timeout,
                                                                           sys_ctl_conf=None,
                                                                           sock_opt_conf=sock_opt_client_conf,
                                                                           file_path=source_file,
                                                                           log_folder=log_folder)
            sleep(transfer_timeout)
            self.switch3.stop_capture(process)

            # Check the encryption scheme chosen
            destination_file = os.path.join(log_folder, os.path.basename(source_file))
            self.check_transfer_failure(source_file=source_file, destination_file=destination_file)
        finally:
            MPTCPsecHost.kill_all(log_folder, tcp_server_process, tcp_client_process, attacker_process)
            self.switch3.stop_capture(process)
            self.router.stop_attacks()

    # TODO Check removal of all options to see if it breaks everything or not

    @classmethod
    def tearDownClass(cls):
        """
        Stop the mininet network
        """
        cls.network.stop()
