from mininet.node import Host
from subprocess import Popen
import os

from scripts.packetFormat import ESN_SUBOPTIONS
from sysctl.sysctlVariables import set_sysctl_value, MPTCP_ESN_PREFERENCE_LIST
from sockopts.sockOptions import sock_opt_arg_label, MPTCP_ENCR_APP_NOT_AWARE
from scripts.connect.mptcpesnConnect import log_file as connection_script_log
from scripts.listen.mptcpesnListen import log_file as listen_script_log

LISTEN_SCRIPT = "scripts/listen/mptcpesnListen.py"
CONNECTION_SCRIPT = "scripts/connect/mptcpesnConnect.py"

SYN_FORGING_SCRIPT = "scripts/forgery_mptcp_esn/constructMptcpEsnSyn.py"
SYN_ACK_FORGING_SCRIPT = "scripts/forgery_mptcp_esn/constructMptcpEsnSynAck.py"
SYN_WITH_ACK_FORGING_SCRIPT = "scripts/forgery_mptcp_esn/constructMptcpEsnSynWithAck.py"
SYN_WITH_OLD_ACK_FORGING_SCRIPT = "scripts/forgery_mptcp_esn/constructMptcpEsnSynWithOldAck.py"
SYN_WITH_TCP_ACK_FORGING_SCRIPT = "scripts/forgery_mptcp_esn/constructMptcpEsnSynWithTcpAck.py"

OLD_SYN_FORGING_SCRIPT = "scripts/forgery_mptcp/constructOldSyn.py"
OLD_SYN_ACK_FORGING_SCRIPT = "scripts/forgery_mptcp/constructOldSynAck.py"
OLD_SYN_WITH_NEW_ACK_FORGING_SCRIPT = "scripts/forgery_mptcp/constructOldSynWithMptcpEsnAck.py"

TCP_SYN_FORGING_SCRIPT = "scripts/forgery_tcp/constructTcpSyn.py"
TCP_SYN_ACK_FORGING_SCRIPT = "scripts/forgery_tcp/constructTcpSynAck.py"


class MPTCPesnHost(Host):

    LOG_FILE = "subprocesses.log"

    def remove_firewall_rules(self):
        """
        Remove all the firewall rules that might have been set for this host
        """
        self.cmd(["iptables", "-F"])

    @staticmethod
    def _get_negotiated_value(process, log_folder, log_file):
        """
        :param process:
        :param log_folder:
        :param log_file:
        :return: the encryption scheme negotiated if any and None otherwise
        """
        return_code = process.poll()
        if return_code is None:
            return_code = process.wait()

        full_path_log_file = os.path.join(log_folder, log_file)
        if return_code == 0:
            with open(full_path_log_file, "r") as file_obj:
                return format(int(file_obj.readline()), '02X')
        return None

    def get_server_negotiated_value(self, listen_process, log_folder):
        """
        :param listen_process:
        :param log_folder:
        :return: the encryption scheme negotiated if any and None otherwise
        """
        return self._get_negotiated_value(listen_process, log_folder, listen_script_log)

    def get_client_negotiated_value(self, connect_process, log_folder):
        """
        :param connect_process:
        :param log_folder:
        :return: the encryption scheme negotiated if any and None otherwise
        """
        return self._get_negotiated_value(connect_process, log_folder, connection_script_log)

    @staticmethod
    def _tuple_list_to_argument(list_label, tuple_list):
        """
        :param list_label:
        :param tuple_list:
        :return: a list of arguments that can be passed to a program
        """
        if tuple_list is None:
            return []

        arg_list = []
        for option in tuple_list:
            arg_list.append(list_label + "=" + str(option[0]) + "=" + str(option[1]))
        return arg_list

    def connect(self, log_folder, remote_ip, remote_port, connection_timeout=None, sys_ctl_conf=None,
                sock_opt_conf=None):
        """
        Launch an asynchronous connection (with encryption)

        :param log_folder:
        :param remote_ip:
        :param remote_port:
        :param connection_timeout:
        :param sys_ctl_conf:
        :param sock_opt_conf:
        :return: the process launched
        """
        if sys_ctl_conf is not None:
            for sys_ctl in sys_ctl_conf:
                if sys_ctl[0] == MPTCP_ESN_PREFERENCE_LIST:
                    set_sysctl_value(sys_ctl[0], "".join(sys_ctl[1]))

        cmd = ["python", CONNECTION_SCRIPT, remote_ip, str(remote_port), str(connection_timeout), log_folder]
        cmd.extend(self._tuple_list_to_argument(sock_opt_arg_label, sock_opt_conf))

        return self.popen(cmd, universal_newlines=True)

    def listen(self, log_folder, local_ip, local_port=9999, sys_ctl_conf=None, sock_opt_conf=None):
        """
        Launch an asynchronous server (with encryption)

        :param log_folder:
        :param local_ip:
        :param local_port:
        :param sys_ctl_conf:
        :param sock_opt_conf:
        :return: the process launched
        """
        if sys_ctl_conf is not None:
            for sys_ctl in sys_ctl_conf:
                if sys_ctl[0] == MPTCP_ESN_PREFERENCE_LIST:
                    set_sysctl_value(sys_ctl[0], "".join(sys_ctl[1]))

        cmd = ["python", LISTEN_SCRIPT, local_ip, str(local_port), log_folder]
        cmd.extend(self._tuple_list_to_argument(sock_opt_arg_label, sock_opt_conf))

        return self.popen(cmd, universal_newlines=True)

    def forge_mptcp_esn_syn(self, remote_ip, remote_port, local_ip=None, local_port=9999,
                            application_aware=MPTCP_ENCR_APP_NOT_AWARE, forged_syn_subopts=ESN_SUBOPTIONS):
        """
        Start an asynchronous sending of a forged SYN segment (for MPTCPesn)

        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        :param application_aware:
        :param forged_syn_subopts:
        :return: the process launched and the list of suboptions used
        """

        if local_ip is None:
            local_ip = self.IP()

        if forged_syn_subopts is None:
            forged_syn_subopts = ESN_SUBOPTIONS

        cmd = ["python", SYN_FORGING_SCRIPT, remote_ip, str(remote_port), local_ip, str(local_port),
               str(application_aware), "".join(forged_syn_subopts)]

        return self.popen(cmd, universal_newlines=True), forged_syn_subopts

    def forge_mptcp_esn_syn_ack(self, interface=None, local_ip=None, local_port=9999,
                                application_aware=MPTCP_ENCR_APP_NOT_AWARE, forged_syn_ack_subopts=ESN_SUBOPTIONS):
        """
        Start an asynchronous script that will reply to a SYN by a forged SYN+ACK (for MPTCPesn)
        The SYN must be have ip_dst=local_ip and port_dst=local_port

        :param interface:
        :param local_ip:
        :param local_port:
        :param application_aware:
        :param forged_syn_ack_subopts:
        :return: the process launched and the list of suboptions used
        """

        if local_ip is None:
            local_ip = self.IP()

        if interface is None:
            interface = self.intf()

        if forged_syn_ack_subopts is None:
            forged_syn_ack_subopts = ESN_SUBOPTIONS

        cmd = ["python", SYN_ACK_FORGING_SCRIPT, interface.name, local_ip, str(local_port), str(application_aware),
               "".join(forged_syn_ack_subopts)]

        return self.popen(cmd, universal_newlines=True)

    def forge_mptcp_esn_syn_with_ack(self, remote_ip, remote_port, local_ip=None, local_port=9999,
                                     application_aware=MPTCP_ENCR_APP_NOT_AWARE, forged_syn_subopts=ESN_SUBOPTIONS):
        """
        Start an asynchronous script that will forge a SYN and then reply to the SYN+ACK with an ACK (for MPTCPesn)

        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        :param application_aware:
        :param forged_syn_subopts:
        :return: the process launched and the list of suboptions used
        """

        if local_ip is None:
            local_ip = self.IP()

        if forged_syn_subopts is None:
            forged_syn_subopts = ESN_SUBOPTIONS

        cmd = ["python", SYN_WITH_ACK_FORGING_SCRIPT, remote_ip, str(remote_port), local_ip, str(local_port),
               str(application_aware), "".join(forged_syn_subopts)]

        return self.popen(cmd, universal_newlines=True), forged_syn_subopts

    def forge_old_syn(self, remote_ip, remote_port, local_ip=None, local_port=9999):
        """
        Start an asynchronous sending of a forged SYN segment (for regular MPTCP)

        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        :return: the process launched
        """

        if local_ip is None:
            local_ip = self.IP()

        cmd = ["python", OLD_SYN_FORGING_SCRIPT, remote_ip, str(remote_port), local_ip, str(local_port)]

        return self.popen(cmd, universal_newlines=True)

    def forge_old_syn_ack(self, interface=None, local_ip=None, local_port=9999):
        """
        Start an asynchronous script that will reply to a SYN by a forged SYN+ACK (for regular MPTCP)
        The SYN must be have ip_dst=local_ip and port_dst=local_port

        :param interface:
        :param local_ip:
        :param local_port:
        :return: the process launched and the list of suboptions used
        """

        if local_ip is None:
            local_ip = self.IP()

        if interface is None:
            interface = self.intf()

        cmd = ["python", OLD_SYN_ACK_FORGING_SCRIPT, interface.name, local_ip, str(local_port)]

        return self.popen(cmd, universal_newlines=True)

    def forge_mptcp_esn_syn_with_old_ack(self, remote_ip, remote_port, local_ip=None, local_port=9999,
                                         application_aware=MPTCP_ENCR_APP_NOT_AWARE, forged_syn_subopts=ESN_SUBOPTIONS):
        """
        Start an asynchronous script that will forge a SYN and then reply to the SYN+ACK with an ACK (for regular MPTCP)

        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        :param application_aware:
        :param forged_syn_subopts:
        :return: the process launched and the list of suboptions used
        """

        if local_ip is None:
            local_ip = self.IP()

        if forged_syn_subopts is None:
            forged_syn_subopts = ESN_SUBOPTIONS

        cmd = ["python", SYN_WITH_OLD_ACK_FORGING_SCRIPT, remote_ip, str(remote_port), local_ip, str(local_port),
               str(application_aware), "".join(forged_syn_subopts)]

        return self.popen(cmd, universal_newlines=True), forged_syn_subopts

    def forge_old_syn_with_new_ack(self, remote_ip, remote_port, local_ip=None, local_port=9999):
        """
        Start an asynchronous script that will forge an old SYN and then reply to the SYN+ACK with a new ACK

        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        :return: the process launched
        """

        if local_ip is None:
            local_ip = self.IP()

        cmd = ["python", OLD_SYN_WITH_NEW_ACK_FORGING_SCRIPT, remote_ip, str(remote_port), local_ip, str(local_port)]

        return self.popen(cmd, universal_newlines=True)

    def forge_tcp_syn(self, remote_ip, remote_port, local_ip=None, local_port=9999):
        """
        Start an asynchronous sending of a forged SYN segment (for TCP)

        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        :return: the process launched
        """

        if local_ip is None:
            local_ip = self.IP()

        cmd = ["python", TCP_SYN_FORGING_SCRIPT, remote_ip, str(remote_port), local_ip, str(local_port)]

        return self.popen(cmd, universal_newlines=True)

    def forge_tcp_syn_ack(self, interface=None, local_ip=None, local_port=9999):
        """
        Start an asynchronous script that will reply to a SYN by a forged SYN+ACK (for TCP)
        The SYN must be have ip_dst=local_ip and port_dst=local_port

        :param interface:
        :param local_ip:
        :param local_port:
        :return: the process launched and the list of suboptions used
        """

        if local_ip is None:
            local_ip = self.IP()

        if interface is None:
            interface = self.intf()

        cmd = ["python", TCP_SYN_ACK_FORGING_SCRIPT, interface.name, local_ip, str(local_port)]

        return self.popen(cmd, universal_newlines=True)

    def forge_mptcp_esn_syn_with_tcp_ack(self, remote_ip, remote_port, local_ip=None, local_port=9999,
                                         application_aware=MPTCP_ENCR_APP_NOT_AWARE, forged_syn_subopts=ESN_SUBOPTIONS):
        """
        Start an asynchronous script that will forge a SYN and then reply to the SYN+ACK with an ACK (for regular MPTCP)

        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        :param application_aware:
        :param forged_syn_subopts:
        :return: the process launched and the list of suboptions used
        """

        if local_ip is None:
            local_ip = self.IP()

        if forged_syn_subopts is None:
            forged_syn_subopts = ESN_SUBOPTIONS

        cmd = ["python", SYN_WITH_TCP_ACK_FORGING_SCRIPT, remote_ip, str(remote_port), local_ip, str(local_port),
               str(application_aware), "".join(forged_syn_subopts)]

        return self.popen(cmd, universal_newlines=True), forged_syn_subopts

    @classmethod
    def kill_all(cls, log_folder, *args):
        """
        Kill (abruptly) all the processes (Popen objects) in argument

        :param log_file: The file where to Popen the output of all the processes
        :param args: Some processes (Popen objects)
        """

        log_file = os.path.join(log_folder, cls.LOG_FILE)

        log_file_obj = open(log_file, "a")
        for process in args:
            if process is not None and isinstance(process, Popen):
                if process.poll() is None:
                    process.kill()
                    process.wait()

                stdout, stderr = process.communicate()
                log_file_obj.write("\n\n---------------------------------------------\n\n")
                log_file_obj.write("  ---------------- Output ----------------  \n")
                log_file_obj.write(str(stdout.decode('utf-8')))
                log_file_obj.write("  ---------------- Error ----------------  \n")
                log_file_obj.write(str(stderr.decode('utf-8')))

        log_file_obj.close()
