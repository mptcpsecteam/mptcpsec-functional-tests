import os

from mptcpesnHost import MPTCPesnHost
from sockopts.sockOptions import sock_opt_arg_label
from sysctl.sysctlVariables import MPTCP_ESN_PREFERENCE_LIST, set_sysctl_value


CONNECTION_TRANSFER_FILE_SCRIPT = "scripts/connect/connectTransferFile.py"
LISTEN_SAVE_FILE_SCRIPT = "scripts/listen/listenSaveFile.py"
TLS_CONNECTION_TRANSFER_FILE_SCRIPT = "scripts/connect/tlsConnectTransferFile.py"
TLS_LISTEN_SAVE_FILE_SCRIPT = "scripts/listen/tlsListenSaveFile.py"


class MPTCPsecHost(MPTCPesnHost):

    @staticmethod
    def only_prefix(ip, prefix_len):

        final_ip = ""

        prefix_len = int(prefix_len)

        byte_start = prefix_len / 8
        bit_start = prefix_len % 8

        current_byte = 0
        current_point = 0
        while current_byte != byte_start:
            current_byte += 1
            current_point = ip.find(".", current_point) + 1

        final_ip += ip[:current_point]  # Contains something like "130."

        if bit_start != 0:
            next_point = ip.find(".", current_point)

            byte = int(ip[current_point:next_point])
            byte &= int("0" * (8 - bit_start), 2)

            final_ip += str(byte) + ("." if byte_start < 4 else "")

        byte_start += 1

        while byte_start <= 4:
            final_ip += "0" + ("." if byte_start < 4 else "")
            byte_start += 1

        return final_ip

    def execute_and_print_if_error(self, cmd):
        """
        Try to execute commands and print if the command fails

        :param cmd:
        :return: True if success
        """

        process = self.popen(cmd, universal_newlines=True)
        if process.poll() is None:
            process.wait()

        if process.returncode != 0:

            print("The following command failed to execute with return code " + str(process.returncode) +
                  " : " + " ".join(cmd))
            stdout, stderr = process.communicate()
            print("\n---------------------------------------------\n")
            print("  ---------------- Output ----------------  ")
            print(str(stdout.decode('utf-8')))
            print("  ---------------- Error ----------------  ")
            print(str(stderr.decode('utf-8')))

            return False

        return True

    def setup_routes(self, router_list):
        """
        Try to setup routes for MPTCP

        :param router_list: The router objects that can be gateways for this host
        :return: True if success
        """

        # For each interface, add a new routing table with some rules
        for intf_name, intf in self.nameToIntf.items():

            # Create an alias for the addition table
            with open("/etc/iproute2/rt_tables", "r+") as file_obj:
                number_lines = 0
                alias_found = False
                for line in file_obj:
                    if line.find(intf_name) >= 0:
                        alias_found = True
                    number_lines += 1

                if not alias_found:
                    file_obj.write(str(number_lines) + " " + intf_name + "\n")

            # Find the gateway

            ip_prefix = self.only_prefix(intf.ip, intf.prefixLen)

            gateway_ip = None
            for router in router_list:
                for router_intf_name, router_intf in router.nameToIntf.items():
                    if ip_prefix == self.only_prefix(router_intf.ip, router_intf.prefixLen):
                        gateway_ip = router_intf.ip
                        break

            if gateway_ip is None:
                print("No match between router and host prefixes (" + ip_prefix + ") !")
                return False

            # Add routes

            cmd = ["ip", "route", "add", "table", intf_name, "to", ip_prefix + "/" + intf.prefixLen, "dev", intf_name,
                   "scope", "link"]
            if not self.execute_and_print_if_error(cmd):
                return False

            cmd = ["ip", "route", "add", "table", intf_name, "default", "via", gateway_ip, "dev", intf_name]
            if not self.execute_and_print_if_error(cmd):
                return False

            # Add rules

            cmd = ["ip", "rule", "add", "from", intf.ip + "/" + intf.prefixLen, "table", intf_name]
            if not self.execute_and_print_if_error(cmd):
                return False

        return True

    def connect_and_transfer_file(self, log_folder, file_path, remote_ip, remote_port, connection_timeout=None,
                                  sys_ctl_conf=None, sock_opt_conf=None, mptcp=False):
        """
        Launch an asynchronous connection (with encryption)

        :param log_folder:
        :param file_path:
        :param remote_ip:
        :param remote_port:
        :param connection_timeout:
        :param sys_ctl_conf:
        :param sock_opt_conf:
        :param mptcp: whether regular MPTCP must be used or not
        :return: the process launched
        """
        if sys_ctl_conf is not None:
            for sys_ctl in sys_ctl_conf:
                if sys_ctl[0] == MPTCP_ESN_PREFERENCE_LIST:
                    set_sysctl_value(sys_ctl[0], "".join(sys_ctl[1]))

        cmd = ["python", CONNECTION_TRANSFER_FILE_SCRIPT, remote_ip, str(remote_port), str(connection_timeout),
               log_folder, file_path, str(mptcp)]
        cmd.extend(self._tuple_list_to_argument(sock_opt_arg_label, sock_opt_conf))

        return self.popen(cmd, universal_newlines=True)

    def listen_and_save_file(self, log_folder, file_path, local_ip, local_port=9999, sys_ctl_conf=None,
                             sock_opt_conf=None, mptcp=False):
        """
        Launch an asynchronous server (with encryption)

        :param log_folder:
        :param file_path:
        :param local_ip:
        :param local_port:
        :param sys_ctl_conf:
        :param sock_opt_conf:
        :param mptcp: whether regular MPTCP must be used or not
        :return: the process launched
        """
        if sys_ctl_conf is not None:
            for sys_ctl in sys_ctl_conf:
                if sys_ctl[0] == MPTCP_ESN_PREFERENCE_LIST:
                    set_sysctl_value(sys_ctl[0], "".join(sys_ctl[1]))

        # Infer destination file path
        destination_file = os.path.join(log_folder, os.path.basename(file_path))
        open(destination_file, 'w').close()

        cmd = ["python", LISTEN_SAVE_FILE_SCRIPT, local_ip, str(local_port), log_folder, destination_file, str(mptcp)]
        cmd.extend(self._tuple_list_to_argument(sock_opt_arg_label, sock_opt_conf))

        return self.popen(cmd, universal_newlines=True)

    def tls_connect_and_transfer_file(self, log_folder, file_path, remote_ip, remote_port, connection_timeout=None,
                                      sys_ctl_conf=None, sock_opt_conf=None):
        """
        Launch an asynchronous connection (with TLS)

        :param log_folder:
        :param file_path:
        :param remote_ip:
        :param remote_port:
        :param connection_timeout:
        :param sys_ctl_conf:
        :param sock_opt_conf:
        :return: the process launched
        """
        if sys_ctl_conf is not None:
            for sys_ctl in sys_ctl_conf:
                if sys_ctl[0] == MPTCP_ESN_PREFERENCE_LIST:
                    set_sysctl_value(sys_ctl[0], "".join(sys_ctl[1]))

        cmd = ["python", TLS_CONNECTION_TRANSFER_FILE_SCRIPT, remote_ip, str(remote_port), str(connection_timeout),
               log_folder, file_path]
        cmd.extend(self._tuple_list_to_argument(sock_opt_arg_label, sock_opt_conf))

        return self.popen(cmd, universal_newlines=True)

    def tls_listen_and_save_file(self, log_folder, file_path, local_ip, local_port=9999, sys_ctl_conf=None,
                                 sock_opt_conf=None):
        """
        Launch an asynchronous server (with TLS)

        :param log_folder:
        :param file_path:
        :param local_ip:
        :param local_port:
        :param sys_ctl_conf:
        :param sock_opt_conf:
        :return: the process launched
        """
        if sys_ctl_conf is not None:
            for sys_ctl in sys_ctl_conf:
                if sys_ctl[0] == MPTCP_ESN_PREFERENCE_LIST:
                    set_sysctl_value(sys_ctl[0], "".join(sys_ctl[1]))

        # Infer destination file path
        destination_file = os.path.join(log_folder, os.path.basename(file_path))
        open(destination_file, 'w').close()

        cmd = ["python", TLS_LISTEN_SAVE_FILE_SCRIPT, local_ip, str(local_port), log_folder, destination_file]
        cmd.extend(self._tuple_list_to_argument(sock_opt_arg_label, sock_opt_conf))

        return self.popen(cmd, universal_newlines=True)
