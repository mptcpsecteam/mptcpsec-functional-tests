from mininet.net import Mininet
from subprocess import Popen
from time import sleep
import os

from mininet_topo import SimpleMultiPathTopo
from mininet_host import MPTCPesnHost
from mptcpesnPacketCheck import MPTCPesnPacketTestCase
from sockopts.sockOptions import MPTCP_ENCR_APP_NOT_AWARE

TIMEOUT = 4  # Time (in seconds) between the launching of each command


class MPTCPesnClientServerTestCase(MPTCPesnPacketTestCase):

    topology = None
    network = None
    client = None
    server = None
    log_folder = "mptcpesn/log"

    @classmethod
    def setUpClass(cls):
        """
        Setup the mininet network
        """

        file_obj = open("clean_mininet.log", "w")
        process = Popen(["sudo", "mn", "-c", "-v", "info"], stdout=file_obj, stderr=file_obj)  # Clean mininet files
        process.wait()
        file_obj.close()
        cls.topology = SimpleMultiPathTopo()
        cls.network = Mininet(cls.topology)
        cls.client = cls.network.getNodeByName("client")
        cls.server = cls.network.getNodeByName("server")
        cls.server.setIP("10.0.0.3", intf=cls.server.intf("server-eth1"))
        cls.switch1 = cls.network.getNodeByName("s1")
        cls.switch2 = cls.network.getNodeByName("s2")
        cls.switch3 = cls.network.getNodeByName("s3")
        cls.network.start()
        cls.switch3_intf_to_client = cls.switch3.connectionsTo(cls.client)[0][0].name

    def connect_without_listening(self, sys_ctl_client_conf=None, sock_opt_client_conf=None, log_folder=log_folder):
        """
        This method will perform the following test :
            - Ask a host to send a SYN by initiating a connection to a server that is not started
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "connect_without_listening.pcap")

        # Ask a host to send a SYN by initiating a connection to a server that is not started
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        client_tcp_process = None
        try:
            client_tcp_process = self.client.connect(remote_ip=self.server.IP(), remote_port=server_port,
                                                     connection_timeout=timeout, sys_ctl_conf=sys_ctl_client_conf,
                                                     sock_opt_conf=sock_opt_client_conf, log_folder=log_folder)
            sleep(timeout)
            self.switch3.stop_capture(process)
            self.check_mptcp_esn_syn(capture_file=capture_file, remote_ip=self.server.IP(), remote_port=server_port,
                                     sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=sock_opt_client_conf)
        finally:
            MPTCPesnHost.kill_all(log_folder, client_tcp_process)
            self.switch3.stop_capture(process)

    def forge_with_listening(self, forged_syn_subopts=None, sys_ctl_server_conf=None,
                             application_aware=MPTCP_ENCR_APP_NOT_AWARE, sock_opt_server_conf=None,
                             log_folder=log_folder, must_fail=False):
        """
        This method will perform the following test :
            - Send manually a SYN to a server in order to trigger a SYN+ACK
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "forge_with_listening.pcap")

        # Send manually a SYN to a server in order to trigger a SYN+ACK
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        forge_syn_process = None
        try:
            tcp_server_process = self.server.listen(local_ip=self.server.IP(), local_port=server_port,
                                                    sys_ctl_conf=sys_ctl_server_conf,
                                                    sock_opt_conf=sock_opt_server_conf, log_folder=log_folder)
            sleep(timeout)
            forge_syn_process, esn_suboptions = self.client.forge_mptcp_esn_syn(remote_ip=self.server.IP(),
                                                                                remote_port=server_port,
                                                                                application_aware=application_aware,
                                                                                forged_syn_subopts=forged_syn_subopts)
            sleep(timeout)
            self.switch3.stop_capture(process)

            if not must_fail:
                self.check_mptcp_esn_synack(capture_file=capture_file, esn_suboptions=esn_suboptions,
                                            local_ip=self.server.IP(), local_port=server_port,
                                            sys_ctl_server_conf=sys_ctl_server_conf,
                                            sock_opt_server_conf=sock_opt_server_conf)
            else:
                self.check_rst(capture_file=capture_file, local_ip=self.server.IP(), local_port=server_port)
        finally:
            MPTCPesnHost.kill_all(log_folder, tcp_server_process, forge_syn_process)
            self.switch3.stop_capture(process)

    def forge_syn_ack_subopts(self, sys_ctl_client_conf=None, sock_opt_client_conf=None, forged_syn_ack_subopts=None,
                              application_aware=MPTCP_ENCR_APP_NOT_AWARE, log_folder=log_folder, must_fail=False,
                              syn_regular_mptcp=False):
        """
        This method will perform the following test :
            - Send manually a SYN+ACK to a client in answer to an SYN
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "forge_syn_ack_subopts.pcap")

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        forge_syn_ack_process = None
        client_tcp_process = None
        try:
            forge_syn_ack_process = self.server.forge_mptcp_esn_syn_ack(interface=self.server.intf(),
                                                                        local_ip=self.server.IP(),
                                                                        local_port=server_port,
                                                                        forged_syn_ack_subopts=forged_syn_ack_subopts,
                                                                        application_aware=application_aware)
            sleep(timeout)
            client_tcp_process = self.client.connect(remote_ip=self.server.IP(), remote_port=server_port,
                                                     connection_timeout=timeout, sys_ctl_conf=sys_ctl_client_conf,
                                                     sock_opt_conf=sock_opt_client_conf, log_folder=log_folder)
            sleep(timeout)
            self.switch3.stop_capture(process)

            if not syn_regular_mptcp:
                self.check_mptcp_esn_syn(capture_file=capture_file, remote_ip=self.server.IP(),
                                         remote_port=server_port, sys_ctl_client_conf=sys_ctl_client_conf,
                                         sock_opt_client_conf=sock_opt_client_conf)
            else:
                self.check_mptcp_syn(capture_file=capture_file, remote_ip=self.server.IP(), remote_port=server_port)

            if not must_fail:
                self.check_mptcp_esn_ack(capture_file=capture_file, remote_ip=self.server.IP(),
                                         remote_port=server_port)

                # Check the encryption scheme chosen
                client_encryption_scheme = self.client.get_client_negotiated_value(client_tcp_process,
                                                                                   log_folder=log_folder)
                self.check_encryption_scheme(client_encryption_scheme=client_encryption_scheme,
                                             server_encryption_scheme=client_encryption_scheme,
                                             sys_ctl_client_conf=sys_ctl_client_conf,
                                             sys_ctl_server_conf=forged_syn_ack_subopts)
            else:
                self.check_rst(capture_file=capture_file, remote_ip=self.server.IP(), remote_port=server_port)
        finally:
            MPTCPesnHost.kill_all(log_folder, forge_syn_ack_process, client_tcp_process)
            self.switch3.stop_capture(process)

    def forge_syn_with_ack_subopts(self, forged_syn_subopts=None, sys_ctl_server_conf=None,
                                   application_aware=MPTCP_ENCR_APP_NOT_AWARE, sock_opt_server_conf=None,
                                   log_folder=log_folder):
        """
        This method will perform the following test :
            - Let the whole connection run in order to also test the ACK of the 3-way handshake
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "forge_syn_with_ack_subopts.pcap")

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        forge_syn_process = None
        try:
            tcp_server_process = self.server.listen(local_ip=self.server.IP(), local_port=server_port,
                                                    sys_ctl_conf=sys_ctl_server_conf,
                                                    sock_opt_conf=sock_opt_server_conf, log_folder=log_folder)
            sleep(timeout)
            forge_syn_process, esn_suboptions = \
                self.client.forge_mptcp_esn_syn_with_ack(remote_ip=self.server.IP(), remote_port=server_port,
                                                         application_aware=application_aware,
                                                         forged_syn_subopts=forged_syn_subopts)
            sleep(timeout)
            self.switch3.stop_capture(process)

            self.check_mptcp_esn_synack(capture_file=capture_file, esn_suboptions=esn_suboptions,
                                        local_ip=self.server.IP(), local_port=server_port,
                                        sys_ctl_server_conf=sys_ctl_server_conf,
                                        sock_opt_server_conf=sock_opt_server_conf)

            # Check the encryption scheme chosen
            server_encryption_scheme = self.server.get_server_negotiated_value(tcp_server_process,
                                                                               log_folder=log_folder)
            self.check_encryption_scheme(client_encryption_scheme=server_encryption_scheme,
                                         server_encryption_scheme=server_encryption_scheme,
                                         sys_ctl_client_conf=forged_syn_subopts,
                                         sys_ctl_server_conf=sys_ctl_server_conf)
        finally:
            MPTCPesnHost.kill_all(log_folder, tcp_server_process, forge_syn_process)
            self.switch3.stop_capture(process)

    def connection_establishment(self, sys_ctl_client_conf=None, sys_ctl_server_conf=None, sock_opt_client_conf=None,
                                 sock_opt_server_conf=None, log_folder=log_folder):
        """
        This method will perform the following test :
            - Let the whole connection run in order to also test the ACK of the 3-way handshake
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "connection_establishment.pcap")

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        client_tcp_process = None
        try:
            tcp_server_process = self.server.listen(local_ip=self.server.IP(), local_port=server_port,
                                                    sys_ctl_conf=sys_ctl_server_conf,
                                                    sock_opt_conf=sock_opt_server_conf, log_folder=log_folder)
            sleep(timeout)
            client_tcp_process = self.client.connect(remote_ip=self.server.IP(), remote_port=server_port,
                                                     connection_timeout=timeout, sys_ctl_conf=sys_ctl_client_conf,
                                                     sock_opt_conf=sock_opt_client_conf, log_folder=log_folder)
            sleep(timeout)
            self.switch3.stop_capture(process)

            # Check the encryption scheme chosen
            client_encryption_scheme = self.client.get_client_negotiated_value(client_tcp_process,
                                                                               log_folder=log_folder)
            server_encryption_scheme = self.server.get_server_negotiated_value(tcp_server_process,
                                                                               log_folder=log_folder)

            self.check_mptcp_esn_connection(capture_file=capture_file, server_ip=self.server.IP(),
                                            server_port=server_port, client_encryption_scheme=client_encryption_scheme,
                                            server_encryption_scheme=server_encryption_scheme,
                                            sys_ctl_client_conf=sys_ctl_client_conf,
                                            sock_opt_client_conf=sock_opt_client_conf,
                                            sys_ctl_server_conf=sys_ctl_server_conf,
                                            sock_opt_server_conf=sock_opt_server_conf)
        finally:
            MPTCPesnHost.kill_all(log_folder, tcp_server_process, client_tcp_process)
            self.switch3.stop_capture(process)

    def forge_old_syn_with_listening(self, sys_ctl_server_conf=None, sock_opt_server_conf=None, log_folder=log_folder):
        """
        This method will perform the following test :
            - Send manually a SYN with an old MP_CAPABLE to a server in MPTCP_ENCR_MUST mode
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "forge_old_syn_with_listening.pcap")

        # Send manually a SYN to a server in order to trigger a SYN+ACK
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        forge_syn_process = None
        try:
            tcp_server_process = self.server.listen(local_ip=self.server.IP(), local_port=server_port,
                                                    sys_ctl_conf=sys_ctl_server_conf,
                                                    sock_opt_conf=sock_opt_server_conf, log_folder=log_folder)
            sleep(timeout)
            forge_syn_process = self.client.forge_old_syn(remote_ip=self.server.IP(), remote_port=server_port)
            sleep(timeout)
            self.switch3.stop_capture(process)

            self.check_rst(capture_file=capture_file, local_ip=self.server.IP(), local_port=server_port)
        finally:
            MPTCPesnHost.kill_all(log_folder, tcp_server_process, forge_syn_process)
            self.switch3.stop_capture(process)

    def forge_old_syn_ack(self, sys_ctl_client_conf=None, sock_opt_client_conf=None, log_folder=log_folder):
        """
        This method will perform the following test :
            - Send manually a SYN+ACK with an old MP_CAPABLE to a client in MPTCP_ENCR_MUST mode
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "forge_old_syn_ack.pcap")

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        forge_syn_ack_process = None
        client_tcp_process = None
        try:
            forge_syn_ack_process = self.server.forge_old_syn_ack(interface=self.server.intf(),
                                                                  local_ip=self.server.IP(),
                                                                  local_port=server_port)
            sleep(timeout)
            client_tcp_process = self.client.connect(remote_ip=self.server.IP(), remote_port=server_port,
                                                     connection_timeout=timeout, sys_ctl_conf=sys_ctl_client_conf,
                                                     sock_opt_conf=sock_opt_client_conf, log_folder=log_folder)
            sleep(timeout)
            self.switch3.stop_capture(process)

            self.check_mptcp_esn_syn(capture_file=capture_file, remote_ip=self.server.IP(),
                                     remote_port=server_port, sys_ctl_client_conf=sys_ctl_client_conf,
                                     sock_opt_client_conf=sock_opt_client_conf)
            self.check_rst(capture_file=capture_file, remote_ip=self.server.IP(), remote_port=server_port)
        finally:
            MPTCPesnHost.kill_all(log_folder, forge_syn_ack_process, client_tcp_process)
            self.switch3.stop_capture(process)

    def forge_syn_with_old_ack(self, forged_syn_subopts=None, application_aware=MPTCP_ENCR_APP_NOT_AWARE,
                               sys_ctl_server_conf=None, sock_opt_server_conf=None, log_folder=log_folder):
        """
        This method will perform the following test :
            - Send manually a SYN with a new MP_CAPABLE and then, an old MP_CAPABLE in the ACK
              to a server in MPTCP_ENCR_MUST mode
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "forge_syn_with_old_ack.pcap")

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        forge_syn_process = None
        try:
            tcp_server_process = self.server.listen(local_ip=self.server.IP(), local_port=server_port,
                                                    sys_ctl_conf=sys_ctl_server_conf,
                                                    sock_opt_conf=sock_opt_server_conf, log_folder=log_folder)
            sleep(timeout)
            forge_syn_process, esn_suboptions = \
                self.client.forge_mptcp_esn_syn_with_old_ack(remote_ip=self.server.IP(), remote_port=server_port,
                                                             application_aware=application_aware,
                                                             forged_syn_subopts=forged_syn_subopts)
            sleep(timeout)
            self.switch3.stop_capture(process)

            self.check_mptcp_esn_synack(capture_file=capture_file, esn_suboptions=esn_suboptions,
                                        local_ip=self.server.IP(), local_port=server_port,
                                        sys_ctl_server_conf=sys_ctl_server_conf,
                                        sock_opt_server_conf=sock_opt_server_conf)
            self.check_rst(capture_file=capture_file, local_ip=self.server.IP(), local_port=server_port)
        finally:
            MPTCPesnHost.kill_all(log_folder, tcp_server_process, forge_syn_process)
            self.switch3.stop_capture(process)

    def forge_old_syn_with_new_ack(self, sock_opt_server_conf=None, sys_ctl_server_conf=None, log_folder=log_folder):
        """
        This method will perform the following test :
            - Send manually a SYN with an old MP_CAPABLE and then, a new MP_CAPABLE in the ACK
              to a server
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "forge_old_syn_with_new_ack.pcap")

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        forge_syn_process = None
        try:
            tcp_server_process = self.server.listen(local_ip=self.server.IP(), local_port=server_port,
                                                    sys_ctl_conf=sys_ctl_server_conf,
                                                    sock_opt_conf=sock_opt_server_conf, log_folder=log_folder)
            sleep(timeout)
            forge_syn_process = self.client.forge_old_syn_with_new_ack(remote_ip=self.server.IP(),
                                                                       remote_port=server_port)
            sleep(timeout)
            self.switch3.stop_capture(process)

            self.check_mptcp_synack(capture_file=capture_file, local_ip=self.server.IP(), local_port=server_port)
            self.check_rst(capture_file=capture_file, local_ip=self.server.IP(), local_port=server_port)
        finally:
            MPTCPesnHost.kill_all(log_folder, tcp_server_process, forge_syn_process)
            self.switch3.stop_capture(process)

    def forge_tcp_syn_with_listening(self, sock_opt_server_conf=None, sys_ctl_server_conf=None, log_folder=log_folder,
                                     must_fail=False):
        """
        This method will perform the following test :
            - Send manually a SYN to a server in order to trigger a fallback (or a RST depending on configurations)
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "forge_tcp_syn_with_listening.pcap")

        # Send manually a SYN to a server in order to trigger a SYN+ACK
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        forge_syn_process = None
        try:
            tcp_server_process = self.server.listen(local_ip=self.server.IP(), local_port=server_port,
                                                    sys_ctl_conf=sys_ctl_server_conf,
                                                    sock_opt_conf=sock_opt_server_conf, log_folder=log_folder)
            sleep(timeout)
            forge_syn_process = self.client.forge_tcp_syn(remote_ip=self.server.IP(), remote_port=server_port)
            sleep(timeout)
            self.switch3.stop_capture(process)

            if not must_fail:
                self.check_tcp_synack(capture_file=capture_file, local_ip=self.server.IP(), local_port=server_port)
            else:
                self.check_rst(capture_file=capture_file, local_ip=self.server.IP(), local_port=server_port)
        finally:
            MPTCPesnHost.kill_all(log_folder, tcp_server_process, forge_syn_process)
            self.switch3.stop_capture(process)

    def forge_tcp_syn_ack(self, sys_ctl_client_conf=None, sock_opt_client_conf=None, log_folder=log_folder):
        """
        This method will perform the following test :
            - Send manually a SYN+ACK without MP_CAPABLE to a client in MPTCP_ENCR_MUST mode
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "forge_tcp_syn_ack.pcap")

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        forge_syn_ack_process = None
        client_tcp_process = None
        try:
            forge_syn_ack_process = self.server.forge_tcp_syn_ack(interface=self.server.intf(),
                                                                  local_ip=self.server.IP(),
                                                                  local_port=server_port)
            sleep(timeout)
            client_tcp_process = self.client.connect(remote_ip=self.server.IP(), remote_port=server_port,
                                                     connection_timeout=timeout, sys_ctl_conf=sys_ctl_client_conf,
                                                     sock_opt_conf=sock_opt_client_conf, log_folder=log_folder)
            sleep(timeout)
            self.switch3.stop_capture(process)

            self.check_mptcp_esn_syn(capture_file=capture_file, remote_ip=self.server.IP(),
                                     remote_port=server_port, sys_ctl_client_conf=sys_ctl_client_conf,
                                     sock_opt_client_conf=sock_opt_client_conf)
            self.check_rst(capture_file=capture_file, remote_ip=self.server.IP(), remote_port=server_port)
        finally:
            MPTCPesnHost.kill_all(log_folder, forge_syn_ack_process, client_tcp_process)
            self.switch3.stop_capture(process)

    def forge_syn_with_tcp_ack(self, forged_syn_subopts=None, application_aware=MPTCP_ENCR_APP_NOT_AWARE,
                               sys_ctl_server_conf=None, sock_opt_server_conf=None, log_folder=log_folder):
        """
        This method will perform the following test :
            - Send manually a SYN with a new MP_CAPABLE and then, an ACK without a MP_CAPABLE
              to a server in MPTCP_ENCR_MUST mode
        """
        server_port = 8000
        timeout = TIMEOUT
        capture_file = os.path.join(log_folder, "forge_syn_with_tcp_ack.pcap")

        # Let the whole connection run in order to also test the ACK of the 3-way handshake
        process = self.switch3.start_capture(self.switch3_intf_to_client, capture_file, "tcp")
        tcp_server_process = None
        forge_syn_process = None
        try:
            tcp_server_process = self.server.listen(local_ip=self.server.IP(), local_port=server_port,
                                                    sys_ctl_conf=sys_ctl_server_conf,
                                                    sock_opt_conf=sock_opt_server_conf, log_folder=log_folder)
            sleep(timeout)
            forge_syn_process, esn_suboptions = \
                self.client.forge_mptcp_esn_syn_with_tcp_ack(remote_ip=self.server.IP(), remote_port=server_port,
                                                             application_aware=application_aware,
                                                             forged_syn_subopts=forged_syn_subopts)
            sleep(timeout)
            self.switch3.stop_capture(process)

            self.check_mptcp_esn_synack(capture_file=capture_file, esn_suboptions=esn_suboptions,
                                        local_ip=self.server.IP(), local_port=server_port,
                                        sys_ctl_server_conf=sys_ctl_server_conf,
                                        sock_opt_server_conf=sock_opt_server_conf)
            self.check_rst(capture_file=capture_file, local_ip=self.server.IP(), local_port=server_port)
        finally:
            MPTCPesnHost.kill_all(log_folder, tcp_server_process, forge_syn_process)
            self.switch3.stop_capture(process)

    @classmethod
    def tearDownClass(cls):
        """
        Stop the mininet network
        """
        cls.network.stop()
