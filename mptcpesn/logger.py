import os
from abc import ABCMeta, abstractmethod

from mininet_host import MPTCPsecHost


class Logger:
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def get_log_folder(self):
        return ""

    def _get_and_create_log_folder(self, log_prefix):

        log_folder = os.path.join(self.get_log_folder(), log_prefix)
        if not os.path.exists(log_folder):
            os.makedirs(log_folder)
        log_file = os.path.join(log_folder, MPTCPsecHost.LOG_FILE)
        open(log_file, "w").close()  # Clear the log file
        return log_folder

    def _get_and_create_log_sub_folder(self, function_name, iteration):

        log_folder = os.path.join(self.get_log_folder(), function_name, "step_" + str(iteration))
        if not os.path.exists(log_folder):
            os.makedirs(log_folder)
        log_file = os.path.join(log_folder, MPTCPsecHost.LOG_FILE)
        open(log_file, "w").close()  # Clear the log file
        return log_folder
