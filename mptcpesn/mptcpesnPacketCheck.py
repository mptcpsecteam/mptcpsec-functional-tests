from unittest import TestCase

from scapy.all import *

from scripts.packetFormat import MPTCP_TYPE, ESN_SUBOPTIONS, MP_CAPABLE_SUB_TYPE
from sockopts.sockOptions import MPTCP_APPLICATION_AWARE
from sysctl.sysctlVariables import MPTCP_ESN_PREFERENCE_LIST, MPTCP_VERSION_2, MPTCP_VERSION_1

SYN_FLAG = 2
ACK_FLAG = 16
RST_FLAG = 4


class MPTCPesnPacketTestCase(TestCase):

    def check_mptcp_esn_mp_capable_option(self, pkt, client_application_aware=0,
                                          esn_suboptions_reference=ESN_SUBOPTIONS):
        """
        Search and check the validity of a MP_CAPABLE option (MPTCPesn version) in a SYN segment

        :param pkt:
        :param client_application_aware:
        :param esn_suboptions_reference:
        :return: the list of MPTCPesn suboptions seen
        """

        is_ack = (pkt[TCP].flags & ACK_FLAG) != 0
        is_syn = (pkt[TCP].flags & SYN_FLAG) != 0

        packet_esn_suboptions = []
        general_suboption_found = False
        for option in pkt[TCP].options:
            if option[0] == MPTCP_TYPE:
                mp_capable_content = option[1].encode("hex")
                esn_suboptions = mp_capable_content[4:]

                version = mp_capable_content[1]
                self.assertEqual(MPTCP_VERSION_2, version, msg="The MP_CAPABLE version is incorrect !"
                                                               " The version is " + version + " and the expected one " +
                                                               "is " + MPTCP_VERSION_2)
                subtype = mp_capable_content[0]
                self.assertEqual(MP_CAPABLE_SUB_TYPE, subtype, msg="The MPTCP option is not an MP_CAPABLE !" +
                                                                   "The subtype is " + subtype +
                                                                   " and the expected one is " + MP_CAPABLE_SUB_TYPE)

                if is_syn:
                    self.assertFalse(len(esn_suboptions) < 4,
                                     msg="The packet sent must contain at least the general suboption"
                                     " with another suboption !")
                    self.assertTrue(len(esn_suboptions) % 2 == 0,
                                    msg="Incorrect format of packet ! The number of hexadecimal digit must be even")

                    for i in range(len(esn_suboptions) / 2):

                        current_suboption = esn_suboptions[i*2:i*2+2]

                        if current_suboption[0] != "0":  # Not the general suboption

                            self.assertIn(current_suboption, esn_suboptions_reference,
                                          msg="The suboption " + current_suboption + " sent does not exists !"
                                          if not is_ack else "The suboption " + current_suboption +
                                                             " was not present in SYN !")
                            packet_esn_suboptions.append(current_suboption)
                        else:
                            self.assertFalse(general_suboption_found, msg="Two general suboptions were found !"
                                                                          " The partial list of non-general suboptions"
                                                                          " is : " + str(packet_esn_suboptions))
                            general_suboption_found = True
                            # Extract the field "application aware" of the general suboption
                            packet_application_aware = (int(current_suboption, 16) & 6)/2
                            self.assertEqual(client_application_aware, packet_application_aware,
                                             msg="The application aware field in the general suboption " +
                                                 str(packet_application_aware) + " doesn't match the real one : " +
                                                 str(client_application_aware) + " !")

                    self.assertTrue(general_suboption_found, msg="No general suboption was found")

                else:
                    self.assertTrue(len(esn_suboptions) == 0, msg="The MP_CAPABLE of the ACK contains suboptions")
                    return [subtype]

                break

        return packet_esn_suboptions

    def check_mptcp_esn_syn(self, capture_file, remote_ip, remote_port, local_ip=None, local_port=-1,
                            sys_ctl_client_conf=None, sock_opt_client_conf=None):
        """
        Search and check the validity of a MP_CAPABLE option (MPTCPesn version) in a SYN segment

        :param capture_file:
        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        :param sys_ctl_client_conf:
        :param sock_opt_client_conf:
        :return: the list of MPTCPesn suboptions seen
        """

        client_application_aware = 0

        if sock_opt_client_conf is not None:
            for sock_opt in sock_opt_client_conf:
                if sock_opt[0] == MPTCP_APPLICATION_AWARE:
                    client_application_aware = sock_opt[1]

        client_preference_list = ESN_SUBOPTIONS
        if sys_ctl_client_conf is not None:
            for sys_ctl in sys_ctl_client_conf:
                if sys_ctl[0] == MPTCP_ESN_PREFERENCE_LIST:
                    client_preference_list = sys_ctl[1]

        capture = rdpcap(capture_file)

        packet_esn_suboptions = []

        for pkt in capture:
            if TCP in pkt and pkt[TCP].flags & SYN_FLAG == SYN_FLAG and (local_ip is None or pkt[IP].src == local_ip) \
                    and pkt[IP].dst == remote_ip and (local_port < 0 or pkt[TCP].sport == local_port) \
                    and pkt[TCP].dport == remote_port:
                packet_esn_suboptions = self.check_mptcp_esn_mp_capable_option(pkt, client_application_aware,
                                                                               client_preference_list)
                break

        self.assertFalse(len(packet_esn_suboptions) == 0, msg="No matching SYN segment was found !")

        return packet_esn_suboptions

    def check_mptcp_esn_synack(self, capture_file, esn_suboptions, local_ip, local_port,
                               remote_ip=None, remote_port=-1, sys_ctl_server_conf=None, sock_opt_server_conf=None):
        """
        Search and check the validity of a MP_CAPABLE option (MPTCPesn version) in a SYN+ACK segment

        :param capture_file:
        :param esn_suboptions: MPTCPesn suboptions sent in the SYN segment
        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        :param sys_ctl_server_conf:
        :param sock_opt_server_conf:
        """

        server_application_aware = 0

        if sock_opt_server_conf is not None:
            for sock_opt in sock_opt_server_conf:
                if sock_opt[0] == MPTCP_APPLICATION_AWARE:
                    server_application_aware = sock_opt[1]

        server_preference_list = ESN_SUBOPTIONS
        if sys_ctl_server_conf is not None:
            for sys_ctl in sys_ctl_server_conf:
                if sys_ctl[0] == MPTCP_ESN_PREFERENCE_LIST:
                    server_preference_list = sys_ctl[1]

        capture = rdpcap(capture_file)

        packet_esn_suboptions = []

        for pkt in capture:
            if TCP in pkt and pkt[TCP].flags & (SYN_FLAG+ACK_FLAG) == (SYN_FLAG+ACK_FLAG) \
                    and pkt[IP].src == local_ip and (remote_ip is None or pkt[IP].dst == remote_ip) \
                    and pkt[TCP].sport == local_port and (remote_port < 0 or pkt[TCP].dport == remote_port):
                packet_esn_suboptions = self.check_mptcp_esn_mp_capable_option(pkt, server_application_aware,
                                                                               esn_suboptions_reference=esn_suboptions)
                break

        self.assertFalse(len(packet_esn_suboptions) == 0, msg="No matching SYN+ACK segment was found !")

        # Test if the MPTCPesn suboption list match the server preference list
        for esn_suboptions in packet_esn_suboptions:
            self.assertIn(esn_suboptions, server_preference_list, msg="The server sent in the SYN+ACK the option " +
                                                                      esn_suboptions + " that does not match " +
                                                                      "its preference list : " +
                                                                      str(server_preference_list) + " !")

    def check_mptcp_esn_ack(self, capture_file, remote_ip, remote_port, local_ip=None, local_port=-1):
        """
        Search and check the validity of a MP_CAPABLE option (MPTCPesn version) in a ACK segment

        :param capture_file:
        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        """
        capture = rdpcap(capture_file)

        for pkt in capture:
            if TCP in pkt and pkt[TCP].flags & SYN_FLAG == 0 and pkt[TCP].flags & ACK_FLAG == ACK_FLAG \
                    and (local_ip is None or pkt[IP].src == local_ip) and pkt[IP].dst == remote_ip \
                    and (local_port < 0 or pkt[TCP].sport == local_port) and pkt[TCP].dport == remote_port:
                return_value = self.check_mptcp_esn_mp_capable_option(pkt)
                self.assertTrue(len(return_value) > 0, msg="No ACK of the TCP three-way handshake was found !")
                return

        self.assertTrue(False, msg="No ACK of the TCP three-way handshake was found !")

    def check_rst(self, capture_file, remote_ip=None, remote_port=-1, local_ip=None, local_port=-1):
        """
        Search and check the presence of a RST segment that match the parameters in argument

        :param capture_file:
        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        """
        capture = rdpcap(capture_file)

        for pkt in capture:
            if TCP in pkt and pkt[TCP].flags & RST_FLAG != 0 and (local_ip is None or pkt[IP].src == local_ip) \
                    and (remote_ip is None or pkt[IP].dst == remote_ip) \
                    and (local_port < 0 or pkt[TCP].sport == local_port) \
                    and (remote_port < 0 or pkt[TCP].dport == remote_port):
                return

        self.assertTrue(False, msg="No RST segment was found !")

    def check_encryption_scheme(self, client_encryption_scheme, server_encryption_scheme, sys_ctl_client_conf=None,
                                sys_ctl_server_conf=None):
        """
        Performs the following checks :
        - Check that both encryption schemes are not None and are equal.
        - Check that the scheme negotiated was in both preference lists.

        :param client_encryption_scheme:
        :param server_encryption_scheme:
        :param sys_ctl_client_conf:
        :param sys_ctl_server_conf:
        :return:
        """

        self.assertIsNotNone(client_encryption_scheme, msg="No encryption scheme for the client !")

        self.assertIsNotNone(server_encryption_scheme, msg="No encryption scheme for the server !")

        self.assertEqual(client_encryption_scheme, server_encryption_scheme,
                         msg="The scheme negotiated is not the same on both sides !" +
                             "The scheme of the client is : " + client_encryption_scheme + " !" +
                             "The scheme of the server is : " + server_encryption_scheme + " !")

        server_preference_list = ESN_SUBOPTIONS
        if sys_ctl_server_conf is not None:
            for sys_ctl in sys_ctl_server_conf:
                if sys_ctl[0] == MPTCP_ESN_PREFERENCE_LIST:
                    server_preference_list = sys_ctl[1]

        client_preference_list = ESN_SUBOPTIONS
        if sys_ctl_client_conf is not None:
            for sys_ctl in sys_ctl_client_conf:
                if sys_ctl[0] == MPTCP_ESN_PREFERENCE_LIST:
                    client_preference_list = sys_ctl[1]

        self.assertIn(client_encryption_scheme, client_preference_list,
                      msg="The client's encryption scheme " + client_encryption_scheme +
                          " is not in its preference list : " + str(client_preference_list))

        self.assertIn(server_encryption_scheme, server_preference_list,
                      msg="The server's encryption scheme " + server_encryption_scheme +
                          " is not in its preference list : " + str(server_preference_list))

    def check_mptcp_esn_connection(self, capture_file, server_ip, server_port,
                                   client_encryption_scheme, server_encryption_scheme, sys_ctl_client_conf,
                                   sock_opt_client_conf, sys_ctl_server_conf, sock_opt_server_conf):

        esn_suboptions = self.check_mptcp_esn_syn(capture_file=capture_file, remote_ip=server_ip,
                                                  remote_port=server_port, sys_ctl_client_conf=sys_ctl_client_conf,
                                                  sock_opt_client_conf=sock_opt_client_conf)
        self.check_mptcp_esn_synack(capture_file=capture_file, esn_suboptions=esn_suboptions,
                                    local_ip=server_ip, local_port=server_port,
                                    sys_ctl_server_conf=sys_ctl_server_conf,
                                    sock_opt_server_conf=sock_opt_server_conf)
        self.check_mptcp_esn_ack(capture_file=capture_file, remote_ip=server_ip, remote_port=server_port)

        # Check the encryption scheme chosen
        self.check_encryption_scheme(client_encryption_scheme=client_encryption_scheme,
                                     server_encryption_scheme=server_encryption_scheme,
                                     sys_ctl_client_conf=sys_ctl_client_conf,
                                     sys_ctl_server_conf=sys_ctl_server_conf)

    def check_mptcp_syn(self, capture_file, remote_ip, remote_port, local_ip=None, local_port=-1):
        """
        Search and check the validity of a MP_CAPABLE option (MPTCPesn version) in a SYN segment

        :param capture_file:
        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        :return: the list of MPTCPesn suboptions seen
        """

        capture = rdpcap(capture_file)

        for pkt in capture:
            if TCP in pkt and pkt[TCP].flags & SYN_FLAG == SYN_FLAG and (local_ip is None or pkt[IP].src == local_ip) \
                    and pkt[IP].dst == remote_ip and (local_port < 0 or pkt[TCP].sport == local_port) \
                    and pkt[TCP].dport == remote_port:
                for option in pkt[TCP].options:
                    if option[0] == MPTCP_TYPE:
                        mp_capable_content = option[1].encode("hex")
                        version = mp_capable_content[1]
                        self.assertEqual(MPTCP_VERSION_1, version, msg="The MP_CAPABLE version is incorrect !"
                                                                       " The version is " + version +
                                                                       " and the expected one " + "is " +
                                                                       MPTCP_VERSION_1)
                        return

        self.assertTrue(False, msg="No matching SYN segment was found !")

    def check_mptcp_synack(self, capture_file, local_ip, local_port, remote_ip=None, remote_port=-1):
        """
        Search and check the validity of a MP_CAPABLE option (regular MPTCP version) in a SYN+ACK segment

        :param capture_file:
        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        """

        capture = rdpcap(capture_file)

        for pkt in capture:
            if TCP in pkt and pkt[TCP].flags & (SYN_FLAG+ACK_FLAG) == (SYN_FLAG+ACK_FLAG) \
                    and pkt[IP].src == local_ip and (remote_ip is None or pkt[IP].dst == remote_ip) \
                    and pkt[TCP].sport == local_port and (remote_port < 0 or pkt[TCP].dport == remote_port):
                for option in pkt[TCP].options:
                    if option[0] == MPTCP_TYPE:
                        mp_capable_content = option[1].encode("hex")
                        version = mp_capable_content[1]
                        self.assertEqual(MPTCP_VERSION_1, version, msg="The MP_CAPABLE version is incorrect !"
                                                                       " The version is " + version +
                                                                       " and the expected one " + "is " +
                                                                       MPTCP_VERSION_1)
                        return

        self.assertTrue(False, msg="No matching SYN+ACK segment was found !")

    def check_tcp_synack(self, capture_file, local_ip, local_port, remote_ip=None, remote_port=-1):
        """
        Search and check the presence of a SYN+ACK segment

        :param capture_file:
        :param remote_ip:
        :param remote_port:
        :param local_ip:
        :param local_port:
        """

        capture = rdpcap(capture_file)

        for pkt in capture:
            if TCP in pkt and pkt[TCP].flags & (SYN_FLAG+ACK_FLAG) == (SYN_FLAG+ACK_FLAG) \
                    and pkt[IP].src == local_ip and (remote_ip is None or pkt[IP].dst == remote_ip) \
                    and pkt[TCP].sport == local_port and (remote_port < 0 or pkt[TCP].dport == remote_port):
                return

        self.assertTrue(False, msg="No matching SYN+ACK segment was found !")
