from mptcpesn.mptcpesnClientServerCheck import MPTCPesnClientServerTestCase
from scripts.packetFormat import ESN_SUBOPTIONS
from sysctl.sysctlVariables import MPTCP_ESN_PREFERENCE_LIST, get_value_from_sysctl, set_sysctl_value,\
    MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256, MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P521,\
    MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_Curve25519, MPTCP_VERSION, MPTCP_VERSION_2, MPTCP_VERSION_1
from sockopts.sockOptions import MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_NOT_AWARE, MPTCP_ENCR_APP_AWARE,\
    MPTCP_ENCR_APP_MUST_BE_AWARE, MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_NOT
from logger import Logger


class MPTCPesnTest(MPTCPesnClientServerTestCase, Logger):

    def get_log_folder(self):
        return self.log_folder

    def setUp(self):

        self.mptcp_esn_preference_list_old = get_value_from_sysctl(MPTCP_ESN_PREFERENCE_LIST)
        set_sysctl_value(MPTCP_ESN_PREFERENCE_LIST, "".join(ESN_SUBOPTIONS))

        self.mptcp_version_old = get_value_from_sysctl(MPTCP_VERSION)
        set_sysctl_value(MPTCP_VERSION, MPTCP_VERSION_2)

        # Remove all firewall rules
        self.client.remove_firewall_rules()
        self.server.remove_firewall_rules()

    def test_normal_behaviour(self):
        """
        This method will perform several tests by using hosts with the same configurations.

        The tests performed are the following :
            - Ask a host to send a SYN by initiating a connection to a server that is not started
            - Send manually a SYN to a server in order to trigger a SYN+ACK
            - Let the whole connection run in order to also test the ACK of the 3-way handshake
        """
        log_folder = self._get_and_create_log_folder("normal_behaviour")

        self.connect_without_listening(log_folder=log_folder)
        self.forge_with_listening(log_folder=log_folder)
        self.connection_establishment(log_folder=log_folder)

    def test_normal_behaviour_different_sys_ctl_configurations(self):
        """
        This method will perform several tests by using hosts with different but compatible sysctl configurations.

        The tests performed are the following :
            - Ask a host to send a SYN by initiating a connection to a server that is not started
            - Send manually a SYN to a server in order to trigger a SYN+ACK
            - Send manually a SYN+ACK to a client in answer to an SYN
            - Send manually a SYN and then an ACK in order to try a connection establishment on server side
        """
        log_folder = self._get_and_create_log_folder("normal_sys_ctl")

        # The purpose of this configuration is to test if the connection will correctly identify
        # "MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256" as the only possible choice (even if it isn't the last option sent)
        sys_ctl_client_conf = [(MPTCP_ESN_PREFERENCE_LIST, [MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256,
                                MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P521])]
        sys_ctl_server_conf = [(MPTCP_ESN_PREFERENCE_LIST, [MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256,
                                MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_Curve25519])]
        sock_opt_client_conf = None
        sock_opt_server_conf = None
        forged_application_aware = MPTCP_ENCR_APP_NOT_AWARE
        forged_syn_subopts = [MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256, MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P521]
        forged_syn_ack_subopts = [MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256, MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_Curve25519]

        self.connect_without_listening(sys_ctl_client_conf=sys_ctl_client_conf,
                                       sock_opt_client_conf=sock_opt_client_conf, log_folder=log_folder)

        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=forged_application_aware, log_folder=log_folder)

        # Send manually a SYN+ACK to a client in answer to an SYN
        self.forge_syn_ack_subopts(sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=sock_opt_client_conf,
                                   forged_syn_ack_subopts=forged_syn_ack_subopts, log_folder=log_folder)

        self.forge_syn_with_ack_subopts(forged_syn_subopts=forged_syn_subopts,
                                        sock_opt_server_conf=sock_opt_server_conf,
                                        sys_ctl_server_conf=sys_ctl_server_conf,
                                        application_aware=forged_application_aware, log_folder=log_folder)

    def test_normal_behaviour_different_app_aware_configurations(self):
        """
        This method will perform several tests by using hosts with compatible application aware settings.

        The tests performed are the following :
            - Ask a host to send a SYN by initiating a connection to a server that is not started
            - Send manually a SYN to a server in order to trigger a SYN+ACK
            - Let the whole connection run in order to also test the ACK of the 3-way handshake
        """
        log_prefix = "normal_app_aware"

        # The purpose of this configuration is to test the influence of the application aware bits
        log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, 1)
        sys_ctl_client_conf = None
        sys_ctl_server_conf = None
        sock_opt_client_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_AWARE)]
        sock_opt_server_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_AWARE)]
        forged_application_aware = MPTCP_ENCR_APP_AWARE
        forged_syn_subopts = None

        self.connect_without_listening(sys_ctl_client_conf=sys_ctl_client_conf,
                                       sock_opt_client_conf=sock_opt_client_conf, log_folder=log_sub_folder)
        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=forged_application_aware, log_folder=log_sub_folder)
        self.connection_establishment(sys_ctl_client_conf=sys_ctl_client_conf, sys_ctl_server_conf=sys_ctl_server_conf,
                                      sock_opt_client_conf=sock_opt_client_conf,
                                      sock_opt_server_conf=sock_opt_server_conf, log_folder=log_sub_folder)

        # The purpose of this configuration is to test the influence of the application aware bits
        log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, 2)
        sock_opt_client_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_NOT_AWARE)]
        sock_opt_server_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_AWARE)]
        forged_application_aware = MPTCP_ENCR_APP_NOT_AWARE

        self.connect_without_listening(sys_ctl_client_conf=sys_ctl_client_conf,
                                       sock_opt_client_conf=sock_opt_client_conf, log_folder=log_sub_folder)
        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=forged_application_aware, log_folder=log_sub_folder)
        self.connection_establishment(sys_ctl_client_conf=sys_ctl_client_conf, sys_ctl_server_conf=sys_ctl_server_conf,
                                      sock_opt_client_conf=sock_opt_client_conf,
                                      sock_opt_server_conf=sock_opt_server_conf, log_folder=log_sub_folder)

        # The purpose of this configuration is to test the influence of the application aware bits
        log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, 3)
        sock_opt_client_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_AWARE)]
        sock_opt_server_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_NOT_AWARE)]
        forged_application_aware = MPTCP_ENCR_APP_AWARE

        self.connect_without_listening(sys_ctl_client_conf=sys_ctl_client_conf,
                                       sock_opt_client_conf=sock_opt_client_conf, log_folder=log_sub_folder)
        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=forged_application_aware, log_folder=log_sub_folder)
        self.connection_establishment(sys_ctl_client_conf=sys_ctl_client_conf, sys_ctl_server_conf=sys_ctl_server_conf,
                                      sock_opt_client_conf=sock_opt_client_conf,
                                      sock_opt_server_conf=sock_opt_server_conf, log_folder=log_sub_folder)

        # The purpose of this configuration is to test the influence of the application aware bits
        log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, 4)
        sock_opt_client_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_MUST_BE_AWARE)]
        sock_opt_server_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_MUST_BE_AWARE)]
        forged_application_aware = MPTCP_ENCR_APP_MUST_BE_AWARE

        self.connect_without_listening(sys_ctl_client_conf=sys_ctl_client_conf,
                                       sock_opt_client_conf=sock_opt_client_conf, log_folder=log_sub_folder)
        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=forged_application_aware, log_folder=log_sub_folder)
        self.connection_establishment(sys_ctl_client_conf=sys_ctl_client_conf, sys_ctl_server_conf=sys_ctl_server_conf,
                                      sock_opt_client_conf=sock_opt_client_conf,
                                      sock_opt_server_conf=sock_opt_server_conf, log_folder=log_sub_folder)

        # The purpose of this configuration is to test the influence of the application aware bits
        log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, 5)
        sock_opt_client_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_MUST_BE_AWARE)]
        sock_opt_server_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_AWARE)]
        forged_application_aware = MPTCP_ENCR_APP_MUST_BE_AWARE

        self.connect_without_listening(sys_ctl_client_conf=sys_ctl_client_conf,
                                       sock_opt_client_conf=sock_opt_client_conf, log_folder=log_sub_folder)
        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=forged_application_aware, log_folder=log_sub_folder)
        self.connection_establishment(sys_ctl_client_conf=sys_ctl_client_conf, sys_ctl_server_conf=sys_ctl_server_conf,
                                      sock_opt_client_conf=sock_opt_client_conf,
                                      sock_opt_server_conf=sock_opt_server_conf, log_folder=log_sub_folder)

        # The purpose of this configuration is to test the influence of the application aware bits
        log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, 6)
        sock_opt_client_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_AWARE)]
        sock_opt_server_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_MUST_BE_AWARE)]
        forged_application_aware = MPTCP_ENCR_APP_AWARE

        self.connect_without_listening(sys_ctl_client_conf=sys_ctl_client_conf,
                                       sock_opt_client_conf=sock_opt_client_conf, log_folder=log_sub_folder)
        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=forged_application_aware, log_folder=log_sub_folder)
        self.connection_establishment(sys_ctl_client_conf=sys_ctl_client_conf, sys_ctl_server_conf=sys_ctl_server_conf,
                                      sock_opt_client_conf=sock_opt_client_conf,
                                      sock_opt_server_conf=sock_opt_server_conf, log_folder=log_sub_folder)

    def test_buggy_behaviour_incompatible_security_preferences(self):
        """
        This method will perform several tests by using hosts with incompatible security preferences.

        The tests performed are the following :
            - Send manually a SYN with an old MP_CAPABLE to a server in MPTCP_ENCR_MUST mode
            - Send manually a SYN+ACK with an old MP_CAPABLE to a client in MPTCP_ENCR_MUST mode
            - Send manually a SYN with a new MP_CAPABLE and then, an old MP_CAPABLE in the ACK
              to a server in MPTCP_ENCR_MUST mode
        """
        log_folder = self._get_and_create_log_folder("buggy_security_preference")

        sys_ctl_client_conf = None
        sys_ctl_server_conf = None
        sock_opt_client_conf = None
        sock_opt_server_conf = None
        forged_syn_subopts = None

        self.forge_old_syn_with_listening(sock_opt_server_conf=sock_opt_server_conf,
                                          sys_ctl_server_conf=sys_ctl_server_conf, log_folder=log_folder)
        self.forge_old_syn_ack(sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=sock_opt_client_conf,
                               log_folder=log_folder)
        self.forge_syn_with_old_ack(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                    sys_ctl_server_conf=sys_ctl_server_conf, log_folder=log_folder)

    def test_buggy_behaviour_incompatible_app_aware_configurations(self):
        """
        This method will perform several tests by using hosts with incompatible application aware settings.

        The tests performed are the following :
            - Send manually a SYN with MPTCP_ENCR_APP_MUST_BE_AWARE mode
              to a server in MPTCP_ENCR_MUST and MPTCP_ENCR_APP_NOT_AWARE modes
            - Send manually a SYN with MPTCP_ENCR_APP_NOT_AWARE mode
              to a server in MPTCP_ENCR_MUST and MPTCP_ENCR_APP_MUST_BE_AWARE modes
            - Send manually a SYN+ACK with MPTCP_ENCR_APP_MUST_BE_AWARE mode
              to a client in MPTCP_ENCR_MUST and MPTCP_ENCR_APP_NOT_AWARE modes
            - Send manually a SYN+ACK with MPTCP_ENCR_APP_NOT_AWARE mode
              to a client in MPTCP_ENCR_MUST and MPTCP_ENCR_APP_MUST_BE_AWARE modes

        Both last cases should not happen with correctly implemented hosts but it still should be tested.
        """
        log_prefix = "buggy_app_aware"

        log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, 1)
        sys_ctl_client_conf = None
        sys_ctl_server_conf = None
        sock_opt_client_conf = None
        sock_opt_server_conf = None
        forged_syn_subopts = None
        syn_application_aware = MPTCP_ENCR_APP_MUST_BE_AWARE
        forged_syn_ack_subopts = None
        syn_ack_application_aware = MPTCP_ENCR_APP_MUST_BE_AWARE

        # Send manually a SYN with MPTCP_ENCR_APP_MUST_BE_AWARE mode
        # to a server in MPTCP_ENCR_MUST and MPTCP_ENCR_APP_NOT_AWARE modes
        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=syn_application_aware, log_folder=log_sub_folder, must_fail=True)

        # Send manually a SYN+ACK with MPTCP_ENCR_APP_MUST_BE_AWARE mode
        # to a client in MPTCP_ENCR_MUST and MPTCP_ENCR_APP_NOT_AWARE modes
        self.forge_syn_ack_subopts(sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=sock_opt_client_conf,
                                   forged_syn_ack_subopts=forged_syn_ack_subopts,
                                   application_aware=syn_ack_application_aware, log_folder=log_sub_folder,
                                   must_fail=True)

        log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, 2)
        sock_opt_client_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_MUST_BE_AWARE)]
        sock_opt_server_conf = [(MPTCP_APPLICATION_AWARE, MPTCP_ENCR_APP_MUST_BE_AWARE)]
        syn_application_aware = MPTCP_ENCR_APP_NOT_AWARE
        syn_ack_application_aware = MPTCP_ENCR_APP_NOT_AWARE

        # Send manually a SYN with MPTCP_ENCR_APP_NOT_AWARE mode
        # to a server in MPTCP_ENCR_MUST and MPTCP_ENCR_APP_MUST_BE_AWARE modes
        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=syn_application_aware, log_folder=log_sub_folder, must_fail=True)

        # Send manually a SYN+ACK with MPTCP_ENCR_APP_NOT_AWARE mode
        # to a client in MPTCP_ENCR_MUST and MPTCP_ENCR_APP_MUST_BE_AWARE modes
        self.forge_syn_ack_subopts(sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=sock_opt_client_conf,
                                   forged_syn_ack_subopts=forged_syn_ack_subopts,
                                   application_aware=syn_ack_application_aware, log_folder=log_sub_folder,
                                   must_fail=True)

    def test_buggy_behaviour_wrong_suboption_list(self):
        """
        This method will perform will forge segments with buggy list of MPTCPesn.
        The host will be configured with MPTCP_ENCR_MUST, thus it should respond by a RST segment.

        The tests performed are the following :
            - Send manually a SYN to a server in order to trigger a RST
            - Send manually a SYN+ACK to a client in order to trigger a RST
        """
        log_prefix = "wrong_suboption_list"

        # Configuration with more than one general suboption
        log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, 1)
        sys_ctl_client_conf = None
        sys_ctl_server_conf = None
        sock_opt_client_conf = None
        sock_opt_server_conf = None
        forged_syn_subopts = list(ESN_SUBOPTIONS)
        forged_syn_subopts.append("00")
        syn_application_aware = MPTCP_ENCR_APP_NOT_AWARE
        forged_syn_ack_subopts = list(ESN_SUBOPTIONS)
        forged_syn_ack_subopts.append("00")
        syn_ack_application_aware = MPTCP_ENCR_APP_NOT_AWARE

        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=syn_application_aware, log_folder=log_sub_folder, must_fail=True)

        self.forge_syn_ack_subopts(sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=sock_opt_client_conf,
                                   forged_syn_ack_subopts=forged_syn_ack_subopts,
                                   application_aware=syn_ack_application_aware, log_folder=log_sub_folder,
                                   must_fail=True)

        # Configuration without encryption schemes
        log_sub_folder = self._get_and_create_log_sub_folder(log_prefix, 2)
        forged_syn_subopts = []
        syn_application_aware = MPTCP_ENCR_APP_NOT_AWARE
        forged_syn_ack_subopts = []
        syn_ack_application_aware = MPTCP_ENCR_APP_NOT_AWARE

        self.forge_with_listening(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                  sys_ctl_server_conf=sys_ctl_server_conf,
                                  application_aware=syn_application_aware, log_folder=log_sub_folder, must_fail=True)

        self.forge_syn_ack_subopts(sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=sock_opt_client_conf,
                                   forged_syn_ack_subopts=forged_syn_ack_subopts,
                                   application_aware=syn_ack_application_aware, log_folder=log_sub_folder,
                                   must_fail=True)

    def test_buggy_behaviour_forge_new_with_old_mptcp(self):
        """
        This method will perform several tests by using hosts with incompatible security preferences.

        The tests performed are the following :
            - Send manually a SYN+ACK with a new MP_CAPABLE to a client in MPTCP_ENCR_NOT mode
            - Send manually a SYN with an old MP_CAPABLE and then, a new MP_CAPABLE in the ACK
              to a server in MPTCP_ENCR_NOT mode
        """
        log_folder = self._get_and_create_log_folder("buggy_forge_new_with_old_mptcp")

        sys_ctl_client_conf = None
        sys_ctl_server_conf = None
        sock_opt_client_conf = [(MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_NOT)]
        sock_opt_server_conf = [(MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_NOT)]
        forged_syn_ack_subopts = None
        syn_ack_application_aware = MPTCP_ENCR_APP_NOT_AWARE

        set_sysctl_value(MPTCP_VERSION, MPTCP_VERSION_1)

        self.forge_syn_ack_subopts(sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=sock_opt_client_conf,
                                   forged_syn_ack_subopts=forged_syn_ack_subopts,
                                   application_aware=syn_ack_application_aware, log_folder=log_folder, must_fail=True,
                                   syn_regular_mptcp=True)
        self.forge_old_syn_with_new_ack(sock_opt_server_conf=sock_opt_server_conf,
                                        sys_ctl_server_conf=sys_ctl_server_conf, log_folder=log_folder)

        set_sysctl_value(MPTCP_VERSION, MPTCP_VERSION_2)

    def test_set_sock_opt_must_encrypt_with_old_version_mptcp(self):
        """
        This method will perform several tests by using hosts with the sysctl "net.mptcp.mptcp_version=1".
        And we will try to establish encryption negotiation. This should work by setting a sock option
        to MPTCP_ENCR_MUST.

        The tests performed are the following :
            - Ask a host to send a SYN by initiating a connection to a server that is not started
            - Send manually a SYN to a server in order to trigger a SYN+ACK
        """
        log_folder = self._get_and_create_log_folder("must_encrypt_old_version")

        set_sysctl_value(MPTCP_VERSION, MPTCP_VERSION_1)

        self.connect_without_listening(log_folder=log_folder)
        self.forge_with_listening(log_folder=log_folder)

        set_sysctl_value(MPTCP_VERSION, MPTCP_VERSION_2)

    def test_buggy_behaviour_fallback_to_tcp(self):
        """
        This method will perform several tests by using hosts with incompatible security preferences.

        The tests performed are the following :
            - Send manually a SYN without MP_CAPABLE to a server in MPTCP_ENCR_MUST mode
            - Send manually a SYN+ACK without MP_CAPABLE to a client in MPTCP_ENCR_MUST mode
            - Send manually a SYN with a new MP_CAPABLE and then, an ACK without a MP_CAPABLE
              to a server in MPTCP_ENCR_MUST mode
        """
        log_folder = self._get_and_create_log_folder("buggy_fallback_to_tcp")

        sys_ctl_client_conf = None
        sys_ctl_server_conf = None
        sock_opt_client_conf = None
        sock_opt_server_conf = None
        forged_syn_subopts = None

        self.forge_tcp_syn_with_listening(sock_opt_server_conf=sock_opt_server_conf,
                                          sys_ctl_server_conf=sys_ctl_server_conf, log_folder=log_folder,
                                          must_fail=True)
        self.forge_tcp_syn_ack(sys_ctl_client_conf=sys_ctl_client_conf, sock_opt_client_conf=sock_opt_client_conf,
                               log_folder=log_folder)
        self.forge_syn_with_tcp_ack(forged_syn_subopts=forged_syn_subopts, sock_opt_server_conf=sock_opt_server_conf,
                                    sys_ctl_server_conf=sys_ctl_server_conf, log_folder=log_folder)

    def tearDown(self):

        set_sysctl_value(MPTCP_ESN_PREFERENCE_LIST, self.mptcp_esn_preference_list_old)

        set_sysctl_value(MPTCP_VERSION, self.mptcp_version_old)

        # Remove all firewall rules
        self.client.remove_firewall_rules()
        self.server.remove_firewall_rules()
