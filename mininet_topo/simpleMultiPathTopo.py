from mininet.topo import Topo

from mininet_switch import CaptureSwitch
from mininet_host import MPTCPesnHost


class SimpleMultiPathTopo(Topo):
    """
    The topology built is the following :

    client --- s1 --- s3 ---- server
      |               |
      +----- s2  -----+
    """

    def build(self, **opts):

        host_cls = MPTCPesnHost
        switch_cls = CaptureSwitch

        if self.hopts.get("cls") is not None:
            host_cls = self.hopts["cls"]

        if self.sopts.get("cls") is not None:
            switch_cls = self.sopts["cls"]

        switch1 = self.addSwitch("s1", cls=switch_cls)
        switch2 = self.addSwitch("s2", cls=switch_cls)
        switch3 = self.addSwitch("s3", cls=switch_cls)

        host1 = self.addHost("server", cls=host_cls)
        host2 = self.addHost("client", cls=host_cls)

        self.addLink(host1, switch1, 0, 1)
        self.addLink(host1, switch2, 1, 1)
        self.addLink(host2, switch3, 0, 1)

        self.addLink(switch1, switch3, 2, 2)
        self.addLink(switch2, switch3, 2, 3)
