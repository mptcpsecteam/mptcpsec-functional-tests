from mininet.topo import Topo

from mininet_router import MPTCPsecAttackerRouter
from mininet_switch import CaptureSwitch
from mininet_host import MPTCPesnHost


class RouterMultiPathTopo(Topo):
    """
    The topology built is the following :

    client --- s1 --- router --- s3 --- server
      |                 |
      +------ s2  ------+
    """

    def build(self, **opts):

        host_cls = MPTCPesnHost
        switch_cls = CaptureSwitch

        if self.hopts.get("cls") is not None:
            host_cls = self.hopts["cls"]

        if self.sopts.get("cls") is not None:
            switch_cls = self.sopts["cls"]

        switch1 = self.addSwitch("s1", cls=switch_cls)
        switch2 = self.addSwitch("s2", cls=switch_cls)
        switch3 = self.addSwitch("s3", cls=switch_cls)

        default_ip = "192.168.1.1/24"
        router = self.addNode("router", cls=MPTCPsecAttackerRouter, ip=default_ip)
        self.addLink(switch1, router, intfName2='router-eth1', params2={'ip': default_ip})
        self.addLink(switch2, router, intfName2='router-eth2', params2={'ip': '172.16.0.1/24'})
        self.addLink(switch3, router, intfName2='router-eth3', params2={'ip': '10.0.0.1/24'})

        host1 = self.addHost("server", cls=host_cls, ip='192.168.1.100/24', defaultRoute='via 192.168.1.1')
        host2 = self.addHost("client", cls=host_cls, ip='10.0.0.100/24', defaultRoute='via 10.0.0.1')

        self.addLink(host1, switch1, 0, 2)
        self.addLink(host1, switch2, 1, 2)
        self.addLink(host2, switch3, 0, 2)
