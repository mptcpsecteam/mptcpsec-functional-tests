from mininet.node import Node


DATA_CORRUPTION_SCRIPT = "scripts/mptcpsec_attackers/data_corruption.py"


class LinuxRouter(Node):
    """
        A Node with IP forwarding enabled.
        This class is inspired from a file available
        at https://github.com/mininet/mininet/blob/master/examples/linuxrouter.py
    """

    def config(self, **params):
        super(LinuxRouter, self).config(**params)
        # Enable forwarding on the router
        self.cmd('sysctl net.ipv4.ip_forward=1')

    def terminate(self):
        self.cmd('sysctl net.ipv4.ip_forward=0')
        super(LinuxRouter, self).terminate()


class MPTCPsecAttackerRouter(LinuxRouter):

    def remove_firewall_rules(self):
        """
        Remove all the firewall rules that might have been set for this host
        """
        self.cmd(["iptables", "-F"])

    def stop_attacks(self):
        """
        Resume normal behaviour
        """
        self.cmd('sysctl net.ipv4.ip_forward=1')

    def corrupt_data(self, wait_for_two_subflows=True):
        """
        This function will transform the router into an attacker that will try to modify one segment of the connection.

        :param wait_for_two_subflows: The modification will only occur after two subflows are established.
                                      Thus, the data transfer should still succeed.
        :return: the process launched
        """

        proxy_ip_addresses = ""
        proxy_mac_addresses = ""

        for intf_name in self.intfNames():
            intf = self.intf(intf_name)
            proxy_ip_addresses += "," + str(intf.ip)
            proxy_mac_addresses += "," + str(intf.mac)

        proxy_ip_addresses = proxy_ip_addresses[1:]
        proxy_mac_addresses = proxy_mac_addresses[1:]

        self.cmd('sysctl net.ipv4.ip_forward=0')

        cmd = ["python", DATA_CORRUPTION_SCRIPT, proxy_ip_addresses, proxy_mac_addresses, str(wait_for_two_subflows)]

        return self.popen(cmd, universal_newlines=True)
