from subprocess import check_output, STDOUT, CalledProcessError

MPTCP_VERSION = "net.mptcp.mptcp_version"

MPTCP_VERSION_1 = "1"
MPTCP_VERSION_2 = "2"

MPTCP_ESN_PREFERENCE_LIST = "net.mptcp.mptcp_esn_suboptions"

MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256 = "10"
MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P521 = "11"
MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_Curve25519 = "12"

MPTCP_PATH_MANAGER = "net.mptcp.mptcp_path_manager"
MPTCP_DEFAULT_PATH_MANAGER = "default"
MPTCP_FULLMESH_PATH_MANAGER = "fullmesh"

MPTCP_DSS_CHECKSUM = "net.mptcp.mptcp_checksum"

sys_ctl_arg_label = "sysctl"


def get_value_from_sysctl(variable_name):

    try:
        return check_output(["sysctl", "-n", "-b", variable_name], stderr=STDOUT, universal_newlines=True)

    except CalledProcessError as e:
        print("CalledProcessError for variable_name=" + variable_name)
        print("CalledProcessError return code=" + str(e.returncode))
        print("CalledProcessError message=" + e.message)
        print("CalledProcessError output=" + e.output)
        print("CalledProcessError cmd=" + str(e.cmd))


def set_sysctl_value(variable_name, value):

    try:
        check_output(["sudo", "sysctl", "-w", variable_name + "=" + value], stderr=STDOUT, universal_newlines=True)

    except CalledProcessError as e:
        print("CalledProcessError for value=" + value)
        print("CalledProcessError return code=" + str(e.returncode))
        print("CalledProcessError message=" + e.message)
        print("CalledProcessError output=" + e.output)
        print("CalledProcessError cmd=" + str(e.cmd))
