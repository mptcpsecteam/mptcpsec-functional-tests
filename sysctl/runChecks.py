from unittest import TestCase
from itertools import chain, combinations, permutations

from sysctl.sysctlVariables import MPTCP_ESN_PREFERENCE_LIST, MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256,\
    MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P521, MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_Curve25519, get_value_from_sysctl,\
    set_sysctl_value


class SysCtlGetSet(TestCase):

    def setUp(self):

        self.old_value = get_value_from_sysctl(MPTCP_ESN_PREFERENCE_LIST)

    def test_sysctl_mptcp_esn_preference_list_get(self):

        value = get_value_from_sysctl(MPTCP_ESN_PREFERENCE_LIST)

        value = value
        self.assertTrue(len(value) > 0, msg="The preference list must contain at least one suboption !")
        self.assertTrue(len(value) % 2 == 0, msg="The number of hexadecimal digit must be even !")
        for i in range(0, len(value)/2):
            current_byte = value[i*2:i*2+2]
            self.assertIn(current_byte, [MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256, MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P521,
                                         MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_Curve25519],
                          msg="The esn suboption " + current_byte + " in the preference list doesn't exists !")

    def test_sysctl_mptcp_esn_preference_list_set(self):

        legit_options = [MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256, MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P521,
                         MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_Curve25519]
        # Since "00" is a general suboption, it cannot be in the preference list
        wrong_options = [MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256, MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P521,
                         MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_Curve25519, "00"]
        redundant_options = list(legit_options[:-1])
        redundant_options.append(MPTCP_ESN_MPTCPSEC_TCPCRYPT_ECDHE_P256)

        # Try all possible combinations
        for combi in chain.from_iterable(combinations(legit_options, r+1) for r in range(len(legit_options))):
            for perm in permutations(combi):

                set_sysctl_value(MPTCP_ESN_PREFERENCE_LIST, "".join(perm))
                real_value = get_value_from_sysctl(MPTCP_ESN_PREFERENCE_LIST)

                self.assertEqual(real_value, "".join(perm), msg="Setting the value " + "".join(perm) + " failed !" +
                                                                " The real value is " + real_value)

        # Try some impossible combinations
        for combi in chain.from_iterable(combinations(wrong_options, r+1) for r in range(len(wrong_options)-1)):
            for perm in permutations(combi):

                if "00" in perm:
                    previous_value = get_value_from_sysctl(MPTCP_ESN_PREFERENCE_LIST)
                    set_sysctl_value(MPTCP_ESN_PREFERENCE_LIST, "".join(perm))
                    real_value = get_value_from_sysctl(MPTCP_ESN_PREFERENCE_LIST)

                    self.assertEqual(real_value, previous_value,
                                     msg="Setting the value " + "".join(perm) + " should have failed ! " +
                                         "Indeed, it contains a wrong suboption ! " + previous_value + " != " +
                                         real_value)

        # Twice the same option
        previous_value = get_value_from_sysctl(MPTCP_ESN_PREFERENCE_LIST)
        set_sysctl_value(MPTCP_ESN_PREFERENCE_LIST, "".join(redundant_options))
        real_value = get_value_from_sysctl(MPTCP_ESN_PREFERENCE_LIST)

        self.assertEqual(real_value, previous_value,
                         msg="Setting the value " + "".join(redundant_options) + " should have failed ! " +
                             "Indeed it contains twice the same suboption !")

    def tearDown(self):

        set_sysctl_value(MPTCP_ESN_PREFERENCE_LIST, self.old_value)
