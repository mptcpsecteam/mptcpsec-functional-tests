#!/usr/bin/env python3

import sys
import os
import time

from sshConnection import is_ip_address, connect, sync_directory, send_command, logout
from globalConstants import USER_NAME, TEST_FOLDER, TEST_MPTCP_ESN, TEST_SOCK_OPTS, TEST_SYS_CTL, TEST_MPTCP_SEC


def show_help():
    print("This program takes the following arguments :")
    print("\tThe first argument must be the IP address of the mininet VM")
    print("\tThe second argument must be the password of the mininet VM")
    print("\tThe third argument must be the path to your host keys file (for ssh connection)")

# Arguments parsing
print(str(sys.argv[1]) + " " + str(is_ip_address(sys.argv[1])))
if len(sys.argv) == 1 or sys.argv[1] == "-h" or len(sys.argv) < 4 or not is_ip_address(sys.argv[1]):
    show_help()
    sys.exit(1)

if not is_ip_address(sys.argv[1]):
    print("ERROR : Bad IP address !")
    show_help()
    sys.exit(1)
ip_address = sys.argv[1]

password = sys.argv[2]

if not os.path.isfile(sys.argv[3]):
    print("ERROR : host keys file " + sys.argv[3] + " doesn't exist !")
    show_help()
    sys.exit(1)
host_keys_path = sys.argv[3]


start_execution = time.time()

# Start SSH connection
client = connect(ip_address, USER_NAME, password, host_keys_path)
if client is None:
    sys.exit(1)

# Copy files to VM
sync_directory(client, os.curdir, TEST_FOLDER)

if len(sys.argv) >= 5 and sys.argv[4] == "load_only":
    sys.exit(0)

# Launch each test individually and log the results
for test_subdirectory in [TEST_SOCK_OPTS, TEST_SYS_CTL, TEST_MPTCP_ESN, TEST_MPTCP_SEC]:
    send_command(client, "cd " + TEST_FOLDER + " && sudo python -m unittest " + test_subdirectory)

# Close the SSH connection
logout(client)

execution_time = int(time.time() - start_execution)

print("\nThe whole script took " + str(execution_time) + " seconds.")
