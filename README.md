# MPTCPesn & MPTCPsec : functionality tests #

The purpose of this repository is to confirm the correct functioning of the linux kernel implementation of MPTCPesn and MPTCPsec (see https://bitbucket.org/mptcpsecteam/mptcp for the implementation).

If you want more details about MPTCPesn, MPTCPsec or what tests are run here (without reading the code), please take a look at the following repository : https://bitbucket.org/mptcpsecteam/mptcpsec-report.

### Set up ###

1. Download the mininet VM : you can follow the first two steps of the first option of the instructions at http://mininet.org/download.
1. Log into the VM and update the packets : 
   ```
   sudo apt-get update
   ``` and
   ```
   sudo apt-get upgrade
   ```.
1. On the VM, install *python-pip* : ```sudo apt-get install python-pip```.
1. On the VM, install the packages *scapy* and *cryptography* : ```sudo pip install scapy``` and ```sudo pip install cryptography```.
1. On your machine, install *python3-pip* : ```sudo apt-get install python3-pip```.
1. On your machine, install the package *paramiko* : ```sudo pip3 install paramiko```.

### Run tests ###

On your machine, execute the script *runChecks.py* with the appropriate arguments (see top of this file for the documentation about the arguments). The VM must have already been started.

### Code organization ###

The code is organized as follows :

* On top level, you have some scripts that can be executed on your machine. The most relevant for testing purpose is *runChecks.py*.
* The folders sysctl, sockopts, mptcpesn, mptcpsec contain some TestCase classes from Unit test python framework (see https://docs.python.org/2/library/unittest.html).
* The folders starting by *mininet_* contain some classes extending default type of Mininet API. In order to understand these classes, you may want to take a closer look at the Mininet API first (see https://github.com/mininet/mininet/wiki/Introduction-to-Mininet).
* The folder *script* contains some python scripts that will be executed on the emulated hosts, switches,...
* The folder *cryptography_api* contains all the interactions to the *cryptography* package.