from subprocess import check_call
from time import sleep

from scapy.all import *

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from scripts.packetFormat import MPTCP_TYPE, MP_CAPABLE_SUB_TYPE
from sysctl.sysctlVariables import MPTCP_VERSION_1


def construct_old_syn_ack(pkt):

    remote_ip = pkt[IP].src
    remote_port = pkt[TCP].sport

    # Create the options for the SYN+ACK
    options = []
    for option in pkt[TCP].options:
        if option[0] == 30:
            mp_capable_esn_syn_ack = (MPTCP_TYPE,
                                      (MP_CAPABLE_SUB_TYPE + MPTCP_VERSION_1 + "81" +
                                       "0011223344556677").decode("hex"))
            options.append(mp_capable_esn_syn_ack)

    ip = IP(src=local_ip, dst=remote_ip)
    tcp = TCP(sport=local_port, dport=remote_port, options=options, flags="SA", ack=(pkt[TCP].seq + 1), seq=1)

    send(ip/tcp)  # Require root privileges


if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))

    interface = sys.argv[1]
    local_ip = sys.argv[2]
    local_port = int(sys.argv[3])

    # We prevent the host to send a RST to an incoming SYN
    check_call(["iptables", "-I", "OUTPUT", "-p", "tcp", "--tcp-flags", "ALL", "RST,ACK", "-j", "DROP"])

    # Require root privileges
    sniff(iface=interface, prn=construct_old_syn_ack,
          filter="tcp and tcp[tcpflags] & tcp-syn != 0 and dst host " + local_ip + " and tcp dst port " +
                 str(local_port), store=0, count=1)

    sleep(1)  # Allow the client to retrieve its encryption scheme before receiving a RST

    # We allow the host to send a RST to an incoming SYN again
    check_call(["iptables", "-D", "OUTPUT", "-p", "tcp", "--tcp-flags", "ALL", "RST,ACK", "-j", "DROP"])
