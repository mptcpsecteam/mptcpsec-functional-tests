from scapy.all import *

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from scripts.packetFormat import MPTCP_TYPE, MP_CAPABLE_SUB_TYPE
from sysctl.sysctlVariables import MPTCP_VERSION_1


def construct_old_syn(remote_ip, remote_port, local_ip, local_port):

    mp_capable_esn_syn = (MPTCP_TYPE,
                          (MP_CAPABLE_SUB_TYPE + MPTCP_VERSION_1 + "81" +
                           "0011223344556677").decode("hex"))

    options = [('MSS', 1460), ('SAckOK', ''), ('Timestamp', (254003, 0)),
               ('NOP', None), ('WScale', 9), mp_capable_esn_syn]
    ip = IP(src=local_ip, dst=remote_ip)
    tcp = TCP(sport=local_port, dport=remote_port, options=options, flags="S")

    send(ip/tcp)  # Require root privileges


if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))
    construct_old_syn(sys.argv[1], int(sys.argv[2]), sys.argv[3], int(sys.argv[4]))
