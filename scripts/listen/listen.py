import SocketServer
import socket

from sockopts.sockOptions import MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_MUST


def listen(local_ip, local_port, sock_opt_conf, handler_class):

    server = SocketServer.TCPServer((local_ip, local_port), handler_class, bind_and_activate=False)
    server.allow_reuse_address = True

    security_preferences_set = False
    # Apply all configurations with sock options
    for sock_opt in sock_opt_conf:
        if sock_opt[0] == MPTCP_SECURITY_PREFERENCE:
            security_preferences_set = True
        server.socket.setsockopt(socket.SOL_TCP, sock_opt[0], sock_opt[1])

    if not security_preferences_set:
        server.socket.setsockopt(socket.SOL_TCP, MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_MUST)  # Force encryption

    try:
        server.server_bind()
        server.server_activate()
        server.handle_request()
    finally:
        server.server_close()
