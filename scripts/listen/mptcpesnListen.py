#!/usr/bin/env python

import SocketServer
import socket
import sys
import os
import struct
from time import sleep

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from sockopts.sockOptions import MPTCP_ENCRYPTION_SCHEME_NEGOTIATED
from scripts.scriptUtil import tuple_lists_from_arguments
from scripts.listen.listen import listen


log_file = "listen.log"
log_folder = None


class MPTCPesnHandler(SocketServer.BaseRequestHandler, object):

    encryption_negotiated = False

    def handle(self, output_folder=None):

        conn_socket = self.request

        value = conn_socket.getsockopt(socket.SOL_TCP, MPTCP_ENCRYPTION_SCHEME_NEGOTIATED, 1)
        value = int(struct.unpack("b", value)[0])

        if output_folder is None:
            output_folder = log_folder

        full_path_log_file = os.path.join(output_folder, log_file)

        if value != 0:
            with open(full_path_log_file, "w") as file_obj:
                file_obj.write(str(value))
                os.fsync(file_obj.fileno())
            with open(full_path_log_file, "r") as file_obj:
                print(file_obj.readlines())
            MPTCPesnHandler.encryption_negotiated = True

        sleep(1)  # Allow the client to retrieve its encryption scheme before the end of the connection


if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))

    sock_opt_list = tuple_lists_from_arguments(sys.argv)

    log_folder = sys.argv[3]

    listen(sys.argv[1], int(sys.argv[2]), sock_opt_list, MPTCPesnHandler)
    if MPTCPesnHandler.encryption_negotiated:
        sys.exit(0)
    else:
        sys.exit(1)
