#!/usr/bin/env python

import SocketServer
import os
import ssl
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from scripts.scriptUtil import tuple_lists_from_arguments
from scripts.listen.listenSaveFile import save_file
from scripts.listen.listen import listen


TLS_AES128GCM_CIPHER_LIST = "AES128-GCM-SHA256:DH-RSA-AES128-GCM-SHA256:DH-DSS-AES128-GCM-SHA256:" \
                            "DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:" \
                            "ECDHE-ECDSA-AES128-GCM-SHA256:ADH-AES128-GCM-SHA256"


class TLSHandler(SocketServer.BaseRequestHandler, object):

    encryption_negotiated = False

    def handle(self, output_folder=None):

        conn_socket = self.request

        certificate = os.path.join(os.path.dirname(sys.argv[0]), "server.pem")

        # Since the TCP connection is already setup, the TLS handshake is going to start immediately
        ssl.wrap_socket(conn_socket, keyfile=None, certfile=certificate, server_side=True, cert_reqs=ssl.CERT_NONE,
                        ssl_version=ssl.PROTOCOL_TLSv1, ca_certs=None, do_handshake_on_connect=True,
                        suppress_ragged_eofs=True)

        save_file(conn_socket, save_file_path=file_path)

if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))

    sock_opt_list = tuple_lists_from_arguments(sys.argv)

    log_folder = sys.argv[3]
    file_path = sys.argv[4]

    listen(sys.argv[1], int(sys.argv[2]), sock_opt_list, TLSHandler)
