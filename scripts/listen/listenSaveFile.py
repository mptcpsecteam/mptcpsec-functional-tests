#!/usr/bin/env python

import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from scripts.scriptUtil import tuple_lists_from_arguments
from scripts.listen.listen import listen
from scripts.listen.mptcpesnListen import MPTCPesnHandler


def save_file(sock, save_file_path=None):

    global file_path

    buffer_size = 4096
    number_of_bytes_received = 0

    if save_file_path is not None:
        file_path = save_file_path

    with open(file_path, "w") as file_obj:
        data = sock.recv(buffer_size)
        number_of_bytes_received += len(data)
        file_obj.write(data)
        while len(data) != 0:
            data = sock.recv(buffer_size)
            number_of_bytes_received += len(data)
            file_obj.write(data)

    print("Reception of " + str(number_of_bytes_received) + " bytes.")


class MPTCPsecSaveFileHandler(MPTCPesnHandler):

    def handle(self, output_folder=None):

        super(MPTCPsecSaveFileHandler, self).handle(output_folder=log_folder)
        if not MPTCPesnHandler.encryption_negotiated:  # Failure of the MPTCPesn negotiation
            return

        save_file(self.request)


class MPTCPSaveFileHandler(MPTCPesnHandler):

    def handle(self, output_folder=None):

        save_file(self.request)


if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))

    sock_opt_list = tuple_lists_from_arguments(sys.argv)

    log_folder = sys.argv[3]
    file_path = sys.argv[4]

    if sys.argv[5] == "True":
        listen(sys.argv[1], int(sys.argv[2]), sock_opt_list, MPTCPSaveFileHandler)
    else:
        listen(sys.argv[1], int(sys.argv[2]), sock_opt_list, MPTCPsecSaveFileHandler)

        if MPTCPsecSaveFileHandler.encryption_negotiated:
            sys.exit(0)
        else:
            sys.exit(1)
