from scapy.all import *


def construct_tcp_syn(remote_ip, remote_port, local_ip, local_port):

    options = [('MSS', 1460), ('SAckOK', ''), ('Timestamp', (254003, 0)),
               ('NOP', None), ('WScale', 9)]
    ip = IP(src=local_ip, dst=remote_ip)
    tcp = TCP(sport=local_port, dport=remote_port, options=options, flags="S")

    send(ip/tcp)  # Require root privileges


if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))
    construct_tcp_syn(sys.argv[1], int(sys.argv[2]), sys.argv[3], int(sys.argv[4]))
