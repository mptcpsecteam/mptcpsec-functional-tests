from subprocess import check_call
from time import sleep

from scapy.all import *

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from scripts.packetFormat import MPTCP_TYPE, MP_CAPABLE_SUB_TYPE
from sysctl.sysctlVariables import MPTCP_VERSION_2


def construct_mptcp_esn_syn_ack(pkt):

    remote_ip = pkt[IP].src
    remote_port = pkt[TCP].sport

    # Create the options for the SYN+ACK
    options = []
    mp_capable_esn_syn_ack = (MPTCP_TYPE, (MP_CAPABLE_SUB_TYPE + MPTCP_VERSION_2 + "81" +
                                           "0" + str(2 * application_aware) + suboption_list).decode("hex"))
    options.append(mp_capable_esn_syn_ack)

    ip = IP(src=local_ip, dst=remote_ip)
    tcp = TCP(sport=local_port, dport=remote_port, options=options, flags="SA", ack=(pkt[TCP].seq + 1), seq=1)

    send(ip/tcp)  # Require root privileges


if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))

    local_ip = sys.argv[2]
    local_port = int(sys.argv[3])
    application_aware = int(sys.argv[4])
    if len(sys.argv) > 5:
        suboption_list = sys.argv[5]
    else:
        suboption_list = ""

    # We prevent the host to send a RST to an incoming SYN
    check_call(["iptables", "-I", "OUTPUT", "-p", "tcp", "--tcp-flags", "ALL", "RST,ACK", "-j", "DROP"])

    # Require root privileges
    sniff(iface=sys.argv[1], prn=construct_mptcp_esn_syn_ack,
          filter="tcp and tcp[tcpflags] & tcp-syn != 0 and dst host " + sys.argv[2] + " and tcp dst port " +
                 sys.argv[3], store=0, count=1)

    sleep(1)  # Allow the client to retrieve its encryption scheme before receiving a RST

    # We allow the host to send a RST to an incoming SYN again
    check_call(["iptables", "-D", "OUTPUT", "-p", "tcp", "--tcp-flags", "ALL", "RST,ACK", "-j", "DROP"])
