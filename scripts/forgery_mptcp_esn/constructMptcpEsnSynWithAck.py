from subprocess import check_call
from time import sleep

from scapy.all import *

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from scripts.packetFormat import MPTCP_TYPE, MP_CAPABLE_SUB_TYPE
from sysctl.sysctlVariables import MPTCP_VERSION_2


def construct_mptcp_esn_syn(remote_ip, remote_port, local_ip, local_port, application_aware, suboption_list):

    mp_capable_esn_syn = (MPTCP_TYPE,
                          (MP_CAPABLE_SUB_TYPE + MPTCP_VERSION_2 + "81" +
                           "0" + str(2 * application_aware) + suboption_list).decode("hex"))

    options = [('MSS', 1460), ('SAckOK', ''), ('Timestamp', (243065, 0)),
               ('NOP', None), ('WScale', 9), mp_capable_esn_syn]
    ip = IP(src=local_ip, dst=remote_ip)
    tcp = TCP(sport=local_port, dport=remote_port, options=options, flags="S")

    return sr1(ip/tcp)  # Require root privileges


def construct_mptcp_esn_ack(syn_ack):

    mp_capable_esn_syn = (MPTCP_TYPE, (MP_CAPABLE_SUB_TYPE + MPTCP_VERSION_2 + "81").decode("hex"))

    options = [('NOP', None), ('NOP', None)]

    for option in syn_ack[TCP].options:
        if option[0] == 'Timestamp':
            options.append((option[0], (option[1][1], option[1][0])))
        elif option[0] == MPTCP_TYPE:
            options.append(mp_capable_esn_syn)

    ip = IP(src=syn_ack[IP].dst, dst=syn_ack[IP].src)
    tcp = TCP(sport=syn_ack[TCP].dport, dport=syn_ack[TCP].sport, options=options, flags="A", seq=syn_ack[TCP].ack,
              ack=syn_ack[TCP].seq + 1, window=58)

    send(ip/tcp)  # Require root privileges


if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))

    # We prevent the host to send a RST to an incoming SYN
    check_call(["iptables", "-I", "OUTPUT", "-p", "tcp", "--tcp-flags", "ALL", "RST", "-j", "DROP"])

    reply = construct_mptcp_esn_syn(sys.argv[1], int(sys.argv[2]), sys.argv[3], int(sys.argv[4]), int(sys.argv[5]),
                                    sys.argv[6])
    construct_mptcp_esn_ack(reply)

    sleep(1)  # Allow the server to retrieve its encryption scheme before the end of the connection

    # We allow again the host to send a RST to packets
    check_call(["iptables", "-D", "OUTPUT", "-p", "tcp", "--tcp-flags", "ALL", "RST", "-j", "DROP"])
