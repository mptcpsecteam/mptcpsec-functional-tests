from abc import ABCMeta, abstractmethod
from subprocess import check_call

from scapy.all import *

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

SYN_FLAG = 2
ACK_FLAG = 16
RST_FLAG = 4
FIN_FLAG = 1


class Proxy:
    __metaclass__ = ABCMeta

    def __init__(self, proxy_ip_addresses, proxy_mac_addresses):
        self.proxy_ip_addresses = proxy_ip_addresses
        for ip in proxy_ip_addresses:
            # We prevent the Proxy to send a RST to an incoming SYN
            check_call(["iptables", "-I", "OUTPUT", "-p", "tcp", "--tcp-flags", "ALL", "RST", "-s", ip, "-j", "DROP"])

        self.proxy_mac_addresses = proxy_mac_addresses

        self._current_packet = None
        # Dictionary {(client_ip, server_ip, client_port, server_port): (fully_established, closing), ...}
        self._subflows = {}

        self._initial_subflows_established = False

        self._stop_proxy = False

    def _get_subflow_key(self, src_ip, dst_ip, src_port, dst_port):
        if (src_ip, dst_ip, src_port, dst_port) in self._subflows:
            return src_ip, dst_ip, src_port, dst_port
        elif (dst_ip, src_ip, dst_port, src_port) in self._subflows:
            return dst_ip, src_ip, dst_port, src_port
        return src_ip, dst_ip, src_port, dst_port

    def keep_going(self, pkt):
        key = self._get_subflow_key(pkt[IP].src, pkt[IP].dst, pkt[TCP].sport, pkt[TCP].dport)
        if pkt[TCP].flags & SYN_FLAG != 0:
            if key in self._subflows:
                self._subflows[key] = (True, False)
                self._initial_subflows_established = True
            else:
                self._subflows[key] = (False, False)
        elif pkt[TCP].flags & RST_FLAG != 0:
            if key in self._subflows:
                del self._subflows[key]
        elif pkt[TCP].flags & FIN_FLAG != 0:
            self._subflows = {}  # End of the connection
        return not self._stop_proxy and (len(self._subflows) > 0 or not self._initial_subflows_established)

    @abstractmethod
    def is_modification_needed(self, pkt):
        return False

    @abstractmethod
    def modify_and_send(self, pkt):
        pass

    def handle(self, pkt):
        self._current_packet = pkt
        if self.is_modification_needed(pkt):
            self.modify_and_send(pkt)
        else:  # Simple forwarding (we remove datalink layer)
            tcp = pkt[TCP]
            ip = pkt[IP]
            send(ip/tcp)

    def filter_str(self):
        s = "tcp"
        for mac in self.proxy_mac_addresses:
            s += " and not ether src " + mac
        return s

    def run(self):
        sniff(prn=lambda new_pkt: self.handle(new_pkt), filter=self.filter_str(), store=0,
              stop_filter=lambda new_pkt: not self.keep_going(new_pkt))
        self.shutdown()

    def shutdown(self):
        # We allow again the host to send a RST to packets and to forward packets
        check_call(["sysctl", "net.ipv4.ip_forward=1"])
        for ip in self.proxy_ip_addresses:
            check_call(["iptables", "-I", "OUTPUT", "-p", "tcp", "--tcp-flags", "ALL", "RST", "-s", ip, "-j", "DROP"])
