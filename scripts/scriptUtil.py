from sockopts.sockOptions import sock_opt_arg_label


def tuple_lists_from_arguments(argv):

    tuple_list_sock_opt = []

    for arg in argv:
        first_equal_index = arg.find("=")
        second_equal_index = arg.find("=", first_equal_index+1)
        if first_equal_index >= 0 or second_equal_index > 0:
            if arg[:first_equal_index] == sock_opt_arg_label:
                tuple_list_sock_opt.append((int(arg[first_equal_index+1:second_equal_index]),
                                            int(arg[second_equal_index+1:])))

    return tuple_list_sock_opt
