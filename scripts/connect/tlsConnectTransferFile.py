#!/usr/bin/env python

import os
import socket
import ssl
import sys
import time

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from sockopts.sockOptions import MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_MUST
from scripts.scriptUtil import tuple_lists_from_arguments

TLS_AES128GCM_CIPHER_LIST = "AES128-GCM-SHA256:DH-RSA-AES128-GCM-SHA256:DH-DSS-AES128-GCM-SHA256:" \
                            "DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:" \
                            "ECDHE-ECDSA-AES128-GCM-SHA256:ADH-AES128-GCM-SHA256"


def tls_transfer_file_handler(sock):

    total_number_of_bytes_sent = 0
    with open(file_path, "r") as file_obj:
        total_bytes = 0
        i = 0
        for line in file_obj:
            number_of_bytes_sent = 0
            if i % 1000 == 0:
                time.sleep(1)
            while number_of_bytes_sent != len(line):
                try:
                    number_of_bytes_sent += sock.send(line[number_of_bytes_sent:])
                    total_bytes += number_of_bytes_sent
                except socket.timeout:
                    raise Exception
            total_number_of_bytes_sent += number_of_bytes_sent
            i += 1

    print("Sending of " + str(total_number_of_bytes_sent) + " bytes.")

    return 0


def tls_connect(remote_ip, remote_port, connection_timeout, sock_opt_conf):

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    security_preferences_set = False
    # Apply all configurations with sock options
    for sock_opt in sock_opt_conf:
        if sock_opt[0] == MPTCP_SECURITY_PREFERENCE:
            security_preferences_set = True
        sock.setsockopt(socket.SOL_TCP, sock_opt[0], sock_opt[1])

    if not security_preferences_set:
        sock.setsockopt(socket.SOL_TCP, MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_MUST)  # Force encryption

    ssl_sock = None
    try:
        sock.connect((remote_ip, remote_port))
        time.sleep(2)
        ssl_sock = ssl.wrap_socket(sock, keyfile=None, certfile=None, server_side=False, cert_reqs=ssl.CERT_NONE,
                                   ssl_version=ssl.PROTOCOL_TLSv1, ca_certs=None, do_handshake_on_connect=True,
                                   suppress_ragged_eofs=True)
        time.sleep(2)
        return tls_transfer_file_handler(sock)
    finally:
        if ssl_sock is None:
            sock.close()
        else:
            sock.close()

if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))

    if sys.argv[3] == 'None':
        timeout = None
    else:
        timeout = int(sys.argv[3])

    log_folder = sys.argv[4]
    file_path = sys.argv[5]

    sock_opt_list = tuple_lists_from_arguments(sys.argv)

    sys.exit(tls_connect(sys.argv[1], int(sys.argv[2]), timeout, sock_opt_list))
