import socket

from sockopts.sockOptions import MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_MUST


def connect(remote_ip, remote_port, connection_timeout, sock_opt_conf, handler):

    socket.setdefaulttimeout(connection_timeout)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    security_preferences_set = False
    # Apply all configurations with sock options
    for sock_opt in sock_opt_conf:
        if sock_opt[0] == MPTCP_SECURITY_PREFERENCE:
            security_preferences_set = True
        sock.setsockopt(socket.SOL_TCP, sock_opt[0], sock_opt[1])

    if not security_preferences_set:
        sock.setsockopt(socket.SOL_TCP, MPTCP_SECURITY_PREFERENCE, MPTCP_ENCR_MUST)  # Force encryption

    try:
        sock.connect((remote_ip, remote_port))

        return handler(sock)
    finally:
        sock.close()
