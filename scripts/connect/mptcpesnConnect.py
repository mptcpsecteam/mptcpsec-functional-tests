#!/usr/bin/env python

import socket
import sys
import os
import struct
from time import sleep

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from sockopts.sockOptions import MPTCP_ENCRYPTION_SCHEME_NEGOTIATED
from scripts.scriptUtil import tuple_lists_from_arguments
from scripts.connect.connect import connect


log_file = "tcp_connect.log"


def mptcp_esn_connect_handler(sock, output_folder=None):

    try:
        value = sock.getsockopt(socket.SOL_TCP, MPTCP_ENCRYPTION_SCHEME_NEGOTIATED, 1)
        value = int(struct.unpack("b", value)[0])

        if output_folder is None:
            output_folder = log_folder

        full_path_log_file = os.path.join(output_folder, log_file)

        if value != 0:
            with open(full_path_log_file, "w") as file_obj:
                file_obj.write(str(value))
                os.fsync(file_obj.fileno())
            return 0
        return 1  # This way, we know if negotiation failed
    finally:
        sleep(1)  # Allow the server to retrieve its encryption scheme before the end of the connection


if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))

    if sys.argv[3] == 'None':
        timeout = None
    else:
        timeout = int(sys.argv[3])

    log_folder = sys.argv[4]

    sock_opt_list = tuple_lists_from_arguments(sys.argv)

    sys.exit(connect(sys.argv[1], int(sys.argv[2]), timeout, sock_opt_list, mptcp_esn_connect_handler))
