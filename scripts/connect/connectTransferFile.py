#!/usr/bin/env python

import os
import socket
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from scripts.connect.mptcpesnConnect import mptcp_esn_connect_handler
from scripts.connect.connect import connect
from scripts.scriptUtil import tuple_lists_from_arguments


def transfer_file_handler(sock, transfer_file_path=None):

    global file_path

    if transfer_file_path is not None:
        file_path = transfer_file_path

    total_number_of_bytes_sent = 0
    with open(file_path, "r") as file_obj:
        total_bytes = 0
        for line in file_obj:
            number_of_bytes_sent = 0
            while number_of_bytes_sent != len(line):
                try:
                    number_of_bytes_sent += sock.send(line[number_of_bytes_sent:])
                    total_bytes += number_of_bytes_sent
                except socket.timeout:
                    raise Exception
            total_number_of_bytes_sent += number_of_bytes_sent

    print("Sending of " + str(total_number_of_bytes_sent) + " bytes.")

    return 0


def mptcpsec_transfer_file_handler(sock):

    negotiation_failure = mptcp_esn_connect_handler(sock, output_folder=log_folder)
    if negotiation_failure == 1:  # Failure of the MPTCPesn negotiation
        return 1

    transfer_file_handler(sock)
    return 0


def mptcp_transfer_file_handler(sock):

    transfer_file_handler(sock)
    return 0


if __name__ == "__main__":

    print("Command : " + " ".join(sys.argv))

    if sys.argv[3] == 'None':
        timeout = None
    else:
        timeout = int(sys.argv[3])

    log_folder = sys.argv[4]
    file_path = sys.argv[5]

    sock_opt_list = tuple_lists_from_arguments(sys.argv)

    if sys.argv[6] == "True":
        sys.exit(connect(sys.argv[1], int(sys.argv[2]), timeout, sock_opt_list, mptcp_transfer_file_handler))
    else:
        sys.exit(connect(sys.argv[1], int(sys.argv[2]), timeout, sock_opt_list, mptcpsec_transfer_file_handler))
