import SocketServer
from unittest import TestCase
import socket
import struct

from sockopts import sockOptions


class SockOptGetSet(TestCase):

    host, port = "localhost", 9999

    server = None
    server_socket = None

    @classmethod
    def setUpClass(cls):
        cls.server = SocketServer.TCPServer((cls.host, cls.port),
                                            SocketServer.BaseRequestHandler, bind_and_activate=False)
        cls.server_socket = cls.server.socket

    def test_mptcp_security_preference_get(self):

        value = self.server_socket.getsockopt(socket.SOL_TCP,
                                              sockOptions.MPTCP_SECURITY_PREFERENCE, 1)

        value = int(struct.unpack("b", value)[0])

        self.assertIn(value, [sockOptions.MPTCP_ENCR_NOT, sockOptions.MPTCP_ENCR_TRY,
                              sockOptions.MPTCP_ENCR_MUST])

    def test_mptcp_security_preference_set(self):

        for option_value in [sockOptions.MPTCP_ENCR_NOT, sockOptions.MPTCP_ENCR_TRY,
                             sockOptions.MPTCP_ENCR_RESERVED, sockOptions.MPTCP_ENCR_MUST]:
            self.server_socket.setsockopt(socket.SOL_TCP,
                                          sockOptions.MPTCP_SECURITY_PREFERENCE,
                                          option_value)
            real_value = self.server_socket.getsockopt(socket.SOL_TCP,
                                                       sockOptions.MPTCP_SECURITY_PREFERENCE, 1)
            real_value = int(struct.unpack("b", real_value)[0])

            self.assertEqual(real_value, option_value if option_value != sockOptions.MPTCP_ENCR_RESERVED
                             else sockOptions.MPTCP_ENCR_TRY)

    def test_mptcp_security_activated_get(self):

        value = self.server_socket.getsockopt(socket.SOL_TCP,
                                              sockOptions.MPTCP_SECURITY_ACTIVATED, 1)

        value = int(struct.unpack("b", value)[0])

        self.assertEqual(value, 0)  # No negotiation was started yet

    def mptcp_application_aware_get(self):

        value = self.server_socket.getsockopt(socket.SOL_TCP,
                                              sockOptions.MPTCP_APPLICATION_AWARE, 1)

        value = int(struct.unpack("b", value)[0])

        self.assertIn(value, [sockOptions.MPTCP_ENCR_APP_NOT_AWARE, sockOptions.MPTCP_ENCR_APP_AWARE,
                              sockOptions.MPTCP_ENCR_APP_MUST_BE_AWARE])

    def mptcp_application_aware_set(self):

        for option_value in [sockOptions.MPTCP_ENCR_APP_NOT_AWARE, sockOptions.MPTCP_ENCR_APP_AWARE,
                             sockOptions.MPTCP_ENCR_APP_AWARE_RESERVED, sockOptions.MPTCP_ENCR_APP_MUST_BE_AWARE]:
            self.server_socket.setsockopt(socket.SOL_TCP,
                                          sockOptions.MPTCP_APPLICATION_AWARE,
                                          option_value)
            real_value = self.server_socket.getsockopt(socket.SOL_TCP,
                                                       sockOptions.MPTCP_APPLICATION_AWARE, 1)

            real_value = int(struct.unpack("b", real_value)[0])

            self.assertEqual(real_value, option_value if option_value != sockOptions.MPTCP_ENCR_APP_AWARE_RESERVED
                             else sockOptions.MPTCP_ENCR_APP_NOT_AWARE)

    def test_mptcp_encryption_scheme_negotiated_get(self):

        value = self.server_socket.getsockopt(socket.SOL_TCP,
                                              sockOptions.MPTCP_ENCRYPTION_SCHEME_NEGOTIATED, 1)

        value = int(struct.unpack("b", value)[0])

        self.assertEqual(value, 0)  # No negotiation occurred yet

    @classmethod
    def tearDownClass(cls):

        cls.server.server_close()
