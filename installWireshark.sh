#!/usr/bin/env bash

echo "Go to folder $1"
cd $1

sudo apt-get install libjs-openlayers

sudo gdebi libwiretap0_*.deb libwiretap-dev_*.deb libwscodecs0_*.deb libwsutil0_*.deb libwsutil-dev_*.deb \
libwireshark-data_*.deb libwireshark0_*.deb libwireshark-dev_*.deb wireshark-dev_*.deb tshark_*.deb \
wireshark-common_*.deb wireshark-qt_*.deb wireshark-gtk_*.deb wireshark_*.deb wireshark-dbg_*.deb wireshark-doc_*.deb
