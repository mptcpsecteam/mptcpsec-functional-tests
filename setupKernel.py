#!/usr/bin/env python3

import sys
import os
import re
import multiprocessing
import subprocess
import shutil
import time

from sshConnection import is_ip_address, connect, logout, sync_file, send_command
from globalConstants import USER_NAME, KERNEL_DEB_PACKAGE_FOLDER, ERROR_LOG


def show_help():
    print("This program takes the following arguments :")
    print("\tThe first argument must be the IP address of the mininet VM")
    print("\tThe second argument must be the password of the mininet VM")
    print("\tThe third argument must be the path to your host keys file (for ssh connection)")
    print("\tThe fourth argument is the path to the kernel source folder")
    print("\tThe fifth argument is the number of cores to use for compilation")
    print("\tA version number can be provided as sixth argument. "
          "If not mentioned (or invalid), it will use \"0.1\" or the version number "
          "after the one of a previously created deb package in\"" + KERNEL_DEB_PACKAGE_FOLDER + "\".")


# Arguments parsing
if len(sys.argv) == 1 or sys.argv[1] == "-h" or len(sys.argv) < 6:
    show_help()
    sys.exit(1)

if not is_ip_address(sys.argv[1]):
    print("ERROR : Bad IP address !")
    show_help()
    sys.exit(1)
ip_address = sys.argv[1]

password = sys.argv[2]

if not os.path.isfile(sys.argv[3]):
    print("ERROR : host keys file " + sys.argv[3] + " doesn't exist !")
    show_help()
    sys.exit(1)
host_keys_path = sys.argv[3]

if not os.path.isdir(sys.argv[4]):
    print("ERROR : kernel source folder " + sys.argv[4] + " doesn't exist !")
    show_help()
    sys.exit(1)
kernel_source_folder = sys.argv[4]

if re.match("(?:(?:[1-9][0-9]*)|(?:[0-9]))", sys.argv[5]) is None:
    print("ERROR : invalid number of cores : " + sys.argv[5] + " !")
    show_help()
    sys.exit(1)
core_number = sys.argv[5]

version_num = None
if len(sys.argv) > 6:
    if re.match("[0-9]+(?:\.[0-9]+)+", sys.argv[6]) is None or multiprocessing.cpu_count() >= int(sys.argv[6]):
        print("ERROR : invalid (or too big) number of cores : " + sys.argv[6] + " !")
        show_help()
        sys.exit(1)
    version_num = sys.argv[6]
else:
    max_version = "0.0"
    for root, dirs, files in os.walk(KERNEL_DEB_PACKAGE_FOLDER):
        for file_name in files:
            is_kernel_file = re.match("linux-image.*\.deb", file_name)
            search_result = re.search("_[0-9]+(?:\.[0-9]+)+_", file_name)
            if is_kernel_file is not None and search_result is not None:
                file_version = str(search_result.group())[1:-1]
                if file_version > max_version:
                    max_version = file_version
        break
    last_number_index = max_version.rfind(".") + 1
    version_num = max_version[:last_number_index] + str(int(max_version[last_number_index:]) + 1)


start_execution = time.time()


# Kernel compilation
pwd = os.getcwd()
os.chdir(kernel_source_folder)

print("fakeroot", "make-kpkg", "-j" + core_number, "--initrd", "--revision=" + version_num, "kernel_image")
process = subprocess.Popen(["fakeroot", "make-kpkg", "-j" + core_number,
                            "--initrd", "--revision=" + version_num, "kernel_image"],
                           stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
stdout, stderr = process.communicate()
os.chdir(pwd)

if process.returncode != 0:
    print("The kernel compilation has failed. Its error log will be saved in " + pwd + "/" + ERROR_LOG)
    error_log = open(ERROR_LOG, "w")
    error_log.write(stderr)
    error_log.close()
    sys.exit(1)

print("The kernel compilation has succeeded. Its error log will be saved in " + pwd + "/" + ERROR_LOG)
error_log = open(ERROR_LOG, "w")
error_log.write(stderr)
error_log.close()


# Copy to kernel package folder
deb_package_path = kernel_source_folder
for line in str(stdout).splitlines():
    if re.match("dpkg-deb: building package `.+' in `.+'", line) is not None:
        begin_path_index = line.rfind("`") + 1
        deb_package_path = os.path.join(deb_package_path, line[begin_path_index:-2])  # -2 for \n and '
deb_package_save_path = os.path.join(KERNEL_DEB_PACKAGE_FOLDER, os.path.basename(deb_package_path))

if deb_package_save_path == KERNEL_DEB_PACKAGE_FOLDER:
    print("We haven't found the debian package containing the kernel :/")
    sys.exit(1)

shutil.move(deb_package_path, deb_package_save_path)
print("The debian package has been saved in file " + deb_package_save_path)


# Start SSH connection
client = connect(ip_address, USER_NAME, password, host_keys_path)
if client is None:
    sys.exit(1)


# Transfer to VM
print("Transfer to VM")
send_command(client, "mkdir " + KERNEL_DEB_PACKAGE_FOLDER, silent=True)
remote_package_path = os.path.join(KERNEL_DEB_PACKAGE_FOLDER, os.path.basename(deb_package_save_path))
sync_file(client, deb_package_save_path, remote_package_path)


# Logout from the VM
logout(client)

execution_time = int(time.time() - start_execution)

print("\nThe whole script took " + str(execution_time) + " seconds.")

print("You can install the new kernel by issuing the following command in the VM :")
print("\tsudo dpkg -i " + remote_package_path)
